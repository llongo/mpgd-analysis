import sys

from flask import Flask
from flask import render_template

import pandas as pd
import uproot 

app = Flask(__name__)

parameters = dict()

@app.route("/show/<string:geometry>/<string:run>/<int:event>")
def display(geometry, run, event):
    return render_template("index.html", geometry=geometry, run=run, event=event)

@app.route("/geometry/<name>")
def get_geometry(name):
    geometry_df = pd.read_csv(f"../geometry/setups/{name}.csv")
    return {
        index: detector.to_dict()
        for index,detector in geometry_df.iterrows()
    }

@app.route("/get/<string:run>/<int:event>")
def get_track(run, event):
    trackTree = uproot.open(f"{parameters['track_directory']}/{run}.root")["trackTree"]
    trackParams = trackTree.arrays(
        ["trackSlopeX", "trackSlopeY", "trackInterceptX", "trackInterceptY"],
        entry_start=event, entry_stop=event+1
    )[0]
    rechitParams = trackTree.arrays(
        ["rechitChamber", "rechitGlobalX", "rechitGlobalY"],
        entry_start=event, entry_stop=event+1
    )[0]
    print(rechitParams["rechitChamber"])
    print(rechitParams["rechitGlobalX"])
    print(rechitParams["rechitGlobalY"])
    trackDict = { key:trackParams[key] for key in trackParams.fields }
    trackDict.update({ key:list(rechitParams[key]) for key in rechitParams.fields })
    return trackDict 

if __name__ == "__main__":
    parameters["track_directory"] = sys.argv[1]
    app.run(debug=True, host="0.0.0.0", port=5050)
