# Analysis software

This package contains the analysis of the reconstructed data.

## Requirements

Some of the analysis codes require to access the setup geometry files; export the `MPGD_GEOMETRY_PATH` environment variable to point to the folder containing the geometry files.

For a typical installation you can do this by the command
```bash
export MPGD_GEOMETRY_PATH="[/path/to/repository/]geometry"
```

## How to install the analysis

From this directory, set the most recent Python version and install the package:
```bash
scl enable rh-python38 bash
python3 -m pip install .
```

If you want to make changes to the code and test it without having to re-run the installation, install he package in editable mode:
```bash
python3 -m pip install --editable .
```

## How to run the analysis

After installing the package, run the command `mpgd-analysis` with the `--help` option to list the available commands.
