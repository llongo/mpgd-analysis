import os, sys, pathlib
import argparse
import logging
from tqdm import tqdm

import uproot
import numpy as np
import pandas as pd
import awkward as ak
import scipy
from scipy.optimize import curve_fit
from scipy.stats import binned_statistic

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use(hep.style.ROOT)
plt.rcParams.update({"font.size": 32})
#plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True
hep.cms.label()#, data=<True|False>, lumi=50, year=2017)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=pathlib.Path, help="Input file")
    parser.add_argument('odir', type=pathlib.Path, help="Output directory")
    parser.add_argument("-n", "--events", type=int, default=-1, help="Number of events to display")
    parser.add_argument("-v", "--verbose", action="store_true", help="Activate logging")
    args = parser.parse_args()
    
    os.makedirs(args.odir, exist_ok=True)
    if args.verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    with uproot.open(args.ifile) as track_file:
        track_tree = track_file["outputtree"]
        if args.verbose: track_tree.show()

        logging.debug("Reading tree...")
        events = track_tree.arrays(entry_stop=args.events)

        chambers = np.unique(ak.flatten(events["digiStripChamber"]).to_numpy())
        chambers = np.append(chambers, np.unique(ak.flatten(events["digiPadChamber"]).to_numpy()))
        chambers = np.sort(chambers)
        n_chambers = len(chambers)
        print(chambers)

        for ievt,event in enumerate(events):

            logging.info("Event {}".format(ievt))
            event_fig, event_axs = plt.subplots(figsize=(11*2,8*n_chambers), ncols=2, nrows=n_chambers)

            for ichamber,chamber in enumerate(chambers):
                logging.info("  Chamber {}".format(chamber))

                chamber_filter = event["digiStripChamber"]==chamber
                strips = event["digiStrip"][chamber_filter]
                charge_strips = event["digiStripCharge"][chamber_filter]
                time_strips = event["digiStripTime"][chamber_filter]

                chamber_filter = event["digiPadChamber"]==chamber
                pads_x = event["digiPadX"][chamber_filter]
                pads_y = event["digiPadY"][chamber_filter]
                charge_pads = event["digiPadCharge"][chamber_filter]
                time_pads = event["digiPadTime"][chamber_filter]

                logging.debug("    Strips {}, size {}".format(strips, ak.count(strips)))
                logging.debug("    Pads X {}, size {}".format(pads_x, ak.count(pads_x)))
                logging.debug("    Pads Y {}, size {}".format(pads_y, ak.count(pads_y)))

                if ak.count(strips)>0: # strip detector
                    event_axs[ichamber][0].bar(strips, charge_strips)
                    event_axs[ichamber][0].set_xlim(0, 358)
                    event_axs[ichamber][0].set_xlabel("Strip")
                    event_axs[ichamber][0].set_ylabel("Charge (DAC)")
                    event_axs[ichamber][1].bar(strips, time_strips)
                    event_axs[ichamber][1].set_xlim(0, 358)
                    event_axs[ichamber][1].set_xlabel("Strip")
                    event_axs[ichamber][1].set_ylabel("Time (BX)")
                else: # pad detector
                    event_axs[ichamber][0].scatter(pads_x, pads_y, c=charge_pads, s=800, marker="s")
                    event_axs[ichamber][0].set_xlim(0, 20)
                    event_axs[ichamber][0].set_ylim(0, 20)
                    event_axs[ichamber][0].set_xlabel("Pad x")
                    event_axs[ichamber][0].set_ylabel("Pad y")
                    event_axs[ichamber][1].scatter(pads_x, pads_y, c=time_pads, s=800, marker="s")
                    event_axs[ichamber][1].set_xlim(0, 20)
                    event_axs[ichamber][1].set_ylim(0, 20)
                    event_axs[ichamber][1].set_xlabel("Pad x")
                    event_axs[ichamber][1].set_ylabel("Pad y")
                event_axs[ichamber][0].set_title("Chamber {}".format(chamber))
                event_axs[ichamber][1].set_title("Chamber {}".format(chamber))

            event_fig.tight_layout()
            event_fig.savefig(args.odir/f"event_{ievt}.png")

if __name__=='__main__': main()
