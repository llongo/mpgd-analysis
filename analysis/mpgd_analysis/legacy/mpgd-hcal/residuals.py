import os, sys, pathlib
import argparse
import logging
from tqdm import tqdm

import uproot
import numpy as np
import pandas as pd
import awkward as ak
import scipy
from scipy.optimize import curve_fit
from scipy.stats import binned_statistic

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use(hep.style.ROOT)
plt.rcParams.update({"font.size": 32})
plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True
hep.cms.label()#, data=<True|False>, lumi=50, year=2017)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("ifile", type=pathlib.Path, help="Input file")
    parser.add_argument('odir', type=pathlib.Path, help="Output directory")
    parser.add_argument("-n", "--events", type=int, default=-1, help="Number of events to analyse")
    parser.add_argument("-v", "--verbose", action="store_true", help="Activate logging")
    args = parser.parse_args()
    
    os.makedirs(args.odir, exist_ok=True)
    if args.verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    with uproot.open(args.ifile) as track_file:
        track_tree = track_file["trackTree"]
        if args.verbose: track_tree.show()

        logging.debug("Reading tree...")
        events = track_tree.arrays([
            "rechitChamber", "rechitGlobalX", "rechitGlobalY",
            "prophitChamber", "trackChi2",
            "prophitGlobalX", "prophitGlobalY",
            "rechitClusterSize"
            ], entry_stop=args.events)

        rechits_chamber = events["rechitChamber"]
        rechits_x = events["rechitGlobalX"]
        rechits_y = events["rechitGlobalY"]
        prophits_chamber = events["prophitChamber"]
        prophits_x = events["prophitGlobalX"]
        prophits_y = events["prophitGlobalY"]
        chi2 = events["trackChi2"]

        """ Apply basic filters """
        chi2_filter = (chi2>=0)&(chi2<=1000000)
        prophits_chamber = prophits_chamber[chi2_filter]
        prophits_x = prophits_x[chi2_filter]
        prophits_y = prophits_y[chi2_filter]
        chi2 = chi2[chi2_filter]

        logging.debug("Rechit variables:")
        logging.debug("  Chambers: {}, size {}".format(rechits_chamber, ak.count(rechits_chamber, axis=1)))
        logging.debug("  x: {}, size {}".format(rechits_x, ak.count(rechits_x, axis=1)))
        logging.debug("  y: {}, size {}".format(rechits_y, ak.count(rechits_y, axis=1)))

        logging.debug("Prophit variables:")
        logging.debug("  Chambers: {}, size {}".format(prophits_chamber, ak.count(prophits_chamber, axis=1)))
        logging.debug("  x: {}, size {}".format(prophits_x, ak.count(prophits_x, axis=1)))
        logging.debug("  y: {}, size {}".format(prophits_y, ak.count(prophits_y, axis=1)))

        """ Plot and fit space resolution and rotation for all detectors """
        chambers = [2, 3, 4]
        for tested_chamber in chambers:

            logging.info("Analysing chamber %d", tested_chamber)
            rechits_x_chamber = rechits_x[rechits_chamber==tested_chamber]
            rechits_y_chamber = rechits_y[rechits_chamber==tested_chamber]
            prophits_x_chamber = prophits_x[prophits_chamber==tested_chamber]
            prophits_y_chamber = prophits_y[prophits_chamber==tested_chamber]
            #chi2_chamber = chi2[prophits_chamber==tested_chamber]

            logging.debug("  Rechits x: {}".format(rechits_x_chamber))
            logging.debug("  Rechits y: {}".format(rechits_y_chamber))
            logging.debug("  Prophits x: {}".format(prophits_x_chamber))
            logging.debug("  Prophits y: {}".format(prophits_y_chamber))

            """ Plot rechit and propagated occupancies """
            """chi2_bins, chi2_range = 100, (-0.01,1000000)
            chi2_fig, chi2_ax = plt.subplots(nrows=1, ncols=1, figsize=(11,9))
            _, chi2_edges, _ = chi2_ax.hist(ak.flatten(chi2_chamber), bins=chi2_bins, range=chi2_range)
            chi2_binning = np.diff(chi2_edges).mean()
            chi2_ax.set_xlabel("Track $\chi^2$")
            chi2_ax.set_ylabel(f"Events / {chi2_binning:1.0f}")
            chi2_ofile = args.odir/f"chi2_chamber_{tested_chamber}.png"
            logging.info("  Saving chi2 to %s", chi2_ofile)
            chi2_fig.savefig(chi2_ofile)"""

            """ Plot rechit and propagated occupancies """
            occupancy_bins, occupancy_range = 100, (-100,100)
            occupancy_fig, occupancy_axs = plt.subplots(nrows=2, ncols=2, figsize=(22,18))
            _, occupancy_edges, _ = occupancy_axs[0][0].hist(ak.flatten(rechits_x_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_axs[0][1].hist(ak.flatten(rechits_y_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_axs[1][0].hist(ak.flatten(prophits_x_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_axs[1][1].hist(ak.flatten(prophits_y_chamber), bins=occupancy_bins, range=occupancy_range)
            occupancy_binning = np.diff(occupancy_edges).mean()*1e3

            occupancy_axs[0][0].set_xlabel("Reconstructed x (mm)")
            occupancy_axs[0][1].set_xlabel("Reconstructed y (mm)")
            occupancy_axs[1][0].set_xlabel("Propagated x (mm)")
            occupancy_axs[1][1].set_xlabel("Propagated y (mm)")
            for ax in occupancy_axs.flat: ax.set_ylabel(f"Events / {occupancy_binning:1.0f} µm")

            occupancy_ofile = args.odir/f"occupancy_chamber_{tested_chamber}.png"
            logging.info("  Saving occupancy to %s", occupancy_ofile)
            occupancy_fig.tight_layout()
            occupancy_fig.savefig(occupancy_ofile)
            plt.close(occupancy_fig)

            """ Plot and fit partial track residuals """ 
            rechits_mask = (ak.count(prophits_x_chamber, axis=1)>0)&(ak.count(rechits_x_chamber, axis=1)>0)
            prophits_x_chamber, prophits_y_chamber = ak.flatten(prophits_x_chamber[rechits_mask]), ak.flatten(prophits_y_chamber[rechits_mask])
            rechits_x_chamber, rechits_y_chamber = rechits_x_chamber[rechits_mask], rechits_y_chamber[rechits_mask]
            """print(prophits_x_chamber)
            print(prophits_y_chamber)
            print(rechits_x_chamber)
            print(rechits_y_chamber)"""
            prophits_x_broadcast, rechits_x_broadcast = ak.broadcast_arrays(prophits_x_chamber, rechits_x_chamber)
            prophits_y_broadcast, rechits_y_broadcast = ak.broadcast_arrays(prophits_y_chamber, rechits_y_chamber)
            residuals_x = prophits_x_broadcast - rechits_x_broadcast
            residuals_y = prophits_y_broadcast - rechits_y_broadcast

            residual_bins, residual_range = 50, (-1000, 1000)
            residual_fig, residual_axs = plt.subplots(nrows=1, ncols=2, figsize=(22,9))
            residual_settings = dict(
                    bins=residual_bins, range=residual_range, 
                    histtype="stepfilled", linewidth=2, facecolor="none", edgecolor="k"
                    )
            _, residual_edges, _ = residual_axs[0].hist(ak.min(residuals_x, axis=1), **residual_settings)
            residual_axs[1].hist(ak.min(residuals_y, axis=1), **residual_settings)
            residual_binning = np.diff(residual_edges).mean()*1e3

            residual_axs[0].set_xlabel("Residual x (mm)")
            residual_axs[1].set_xlabel("Residual y (mm)")
            for ax in residual_axs: ax.set_ylabel(f"Events / {residual_binning:1.0f} µm")

            residual_ofile = args.odir/f"residual_chamber_{tested_chamber}.png"
            logging.info("  Saving residuals to %s", residual_ofile)
            residual_fig.tight_layout()
            residual_fig.savefig(residual_ofile)
            plt.close(residual_fig)

            prophit_rechit_fig, prophit_rechit_axs = plt.subplots(nrows=1, ncols=2, figsize=(22,9))
            hit_range = ((-100,100),(-100,100))
            hit_bins = (100,100)
            prophit_rechit_axs[0].hist2d(ak.flatten(prophits_x_broadcast).to_numpy(), ak.flatten(rechits_x_broadcast).to_numpy(), range=hit_range, bins=hit_bins) 
            prophit_rechit_axs[1].hist2d(ak.flatten(prophits_y_broadcast).to_numpy(), ak.flatten(rechits_y_broadcast).to_numpy(), range=hit_range, bins=hit_bins)
            prophit_rechit_fig.tight_layout()
            prophit_rechit_ofile = args.odir/f"prophit_rechit_chamber_{tested_chamber}.png"
            prophit_rechit_fig.savefig(prophit_rechit_ofile)

if __name__=='__main__': main()
