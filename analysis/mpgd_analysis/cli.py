import argparse
import pathlib
 
from mpgd_analysis.qc8 import tracks as qc8_tracks
from mpgd_analysis.qc8 import hv_scan as qc8_hv_scan

def main():
    parser = argparse.ArgumentParser(
            prog="mpgd-analysis",
            description="Analyze MPGD data"
            )
    subparsers = parser.add_subparsers(required=True)

    """ CMS GEM QC8 analysis """
    qc8_parser = subparsers.add_parser("qc8", help="Run analysis for CMS GEM QC8")
    qc8_subparsers = qc8_parser.add_subparsers(required=True)

    """ Analyze QC8 tracks """
    qc8_tracks_parser = qc8_subparsers.add_parser("tracks", help="Analyze QC8 tracks")
    qc8_tracks_parser.add_argument("ifile", type=pathlib.Path, help="Track file")
    qc8_tracks_parser.add_argument("odir", type=pathlib.Path, help="Output directory")
    qc8_tracks_parser.add_argument("--efficiency", action="store_true", help="Calculate efficiency")
    qc8_tracks_parser.add_argument("-n", "--events", type=int, help="Number of events to analyze")
    qc8_tracks_parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    qc8_tracks_parser.set_defaults(
            func = lambda args: qc8_tracks.analyze(args.ifile, args.odir, args.events, args.efficiency, args.verbose)
            )

    """ Plot QC8 efficiency scan """
    qc8_scan_parser = qc8_subparsers.add_parser("hv-scan", help="Plot QC8 HV scan results")
    qc8_scan_parser.add_argument("ifile", type=pathlib.Path, help="Scan input csv file")
    qc8_scan_parser.add_argument("odir", type=pathlib.Path, help="Output directory")
    qc8_scan_parser.add_argument("--efficiency", required=True, type=pathlib.Path, nargs="+", help="Efficiency csv file for each run")
    qc8_scan_parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    qc8_scan_parser.set_defaults(
            func = lambda args: qc8_hv_scan.analyze(args.ifile, args.odir, args.efficiency, args.verbose)
            )


    args = parser.parse_args()
    args.func(args)

if __name__=="__main__":
    main()
