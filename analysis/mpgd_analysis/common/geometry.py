import os

from xml.dom.minidom import parse, parseString

MPGD_GEOMETRY_PATH = os.environ["MPGD_GEOMETRY_PATH"]

class DetectorGeometry:

    def __init__(self, detector_dict, alignment_dict, geometry_dict):
        self.detector_dict = detector_dict
        self.alignment_dict = alignment_dict
        self.geometry_dict = geometry_dict

    @property
    def label(self):
        return self.detector_dict["label"]

    @property
    def baseSmall(self):
        return self.geometry_dict["baseSmall"]

    @property
    def baseLarge(self):
        return self.geometry_dict["baseLarge"]

    @property
    def height(self):
        return self.geometry_dict["height"]

    @property
    def nEta(self):
        return self.geometry_dict["nEta"]

    @property
    def etaHeight(self):
        return self.height / self.nEta

    def getY(self, eta):
        return -0.5*self.height + self.etaHeight*(0.5 + (self.nEta - eta))
 
    def getWidth(self, eta):
        return self.baseSmall + (self.getY(eta) + 0.5*self.height)*(self.baseLarge-self.baseSmall)/self.height

    def __str__(self):
        return "Detector {}\nbase small {}, base large {}, height {}, number of etas {}".format(
                self.label, self.baseSmall, self.baseLarge, self.height, self.nEta
                )

class SetupGeometry:

    def __init__(self, setup_name):

        self.detectors = dict()

        """ Parse the geometry file of the setup """
        document = parse("{}/setups/{}.xml".format(MPGD_GEOMETRY_PATH, setup_name))
        setup_element = document.documentElement
        detector_elements = setup_element.getElementsByTagName("detector")

        for index,detector in enumerate(detector_elements):
            geometry_name = detector.getElementsByTagName("geometry")[0].firstChild.nodeValue
            detector_dict = {
                    "label": detector.getAttribute("label"),
                    "chamber": int(detector.getAttribute("id")),
                    "geometry": geometry_name
                    }

            detector_alignment = detector.getElementsByTagName("alignment")[0]
            alignment_dict = {
                    "x": detector_alignment.getElementsByTagName("x")[0].firstChild.nodeValue,
                    "y": detector_alignment.getElementsByTagName("y")[0].firstChild.nodeValue,
                    "z": detector_alignment.getElementsByTagName("z")[0].firstChild.nodeValue,
                    "phi_z": detector_alignment.getElementsByTagName("phi_z")[0].firstChild.nodeValue
                    }

            """ Parse geometry file of this detector """
            detector_xml = parse("{}/detectors/{}.xml".format(MPGD_GEOMETRY_PATH, geometry_name))
            geometry_elements = detector_xml.documentElement.getElementsByTagName("geometry")[0]
            readout_elements = detector_xml.documentElement.getElementsByTagName("readout")[0]
            geometry_dict = {
                    "baseSmall": float(geometry_elements.getElementsByTagName("baseSmall")[0].firstChild.nodeValue),
                    "baseLarge": float(geometry_elements.getElementsByTagName("baseLarge")[0].firstChild.nodeValue),
                    "height": float(geometry_elements.getElementsByTagName("height")[0].firstChild.nodeValue),
                    "nEta": int(readout_elements.getElementsByTagName("nEta")[0].firstChild.nodeValue)
                    }

            self.detectors[detector_dict["chamber"]] = DetectorGeometry(detector_dict, alignment_dict, geometry_dict)

    def getDetector(self, detector_id):
        return self.detectors[detector_id]

    def __str__(self):
        return "\n".join([
            "Detector {}:\n{}".format(k, str(det))
            for k,det in self.detectors.items()
            ])
