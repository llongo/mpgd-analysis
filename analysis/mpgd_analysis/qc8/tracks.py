import logging
import os

import awkward as ak
import numpy as np
import pandas as pd
import uproot

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import mplhep as hep

plt.style.use(hep.style.ROOT)
#plt.rcParams.update({"font.size": 32})
#plt.rcParams.update({"image.cmap": "Purples"})
logging.getLogger('matplotlib.font_manager').disabled = True

from mpgd_analysis.qc8 import tools as qc8_tools
from mpgd_analysis.common import tools as common_tools
from mpgd_analysis.common import geometry 

def analyze(ifile, odir, nevents, calculate_efficiency, verbose):

    os.makedirs(odir/"residuals", exist_ok=True)
    if calculate_efficiency:
        os.makedirs(odir/"matching-profiles", exist_ok=True)
        os.makedirs(odir/"efficiency-profiles", exist_ok=True)
        os.makedirs(odir/"efficiency-maps", exist_ok=True)
        # Dictionary need to save efficiency to csv:
        efficiency_tuples = list()

    if verbose: logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    else: logging.basicConfig(level=logging.INFO, format="%(message)s")

    """ Load xml geometry files """
    setup = geometry.SetupGeometry("qc8")

    with uproot.open(ifile) as track_file:

        track_tree = track_file["trackTree"]
        events = track_tree.arrays(
            [
            "rechitChamber", "rechitEta",
            "rechitLocalX", "rechitLocalY", "rechitClusterSize",
            "rechitR", "rechitPhi",
            "trackSlopeX", "trackSlopeY",
            "partialTrackChamber", "partialTrackChi2",
            "partialTrackSlopeX", "partialTrackSlopeY",
            "partialProphitGlobalX", "partialProphitGlobalY",
            "partialProphitEta"
            ], entry_stop=nevents
        )

        rechits_chamber = events["rechitChamber"]
        rechits_eta = events["rechitEta"]
        rechits_cluster_size = events["rechitClusterSize"]
        rechits_x = events["rechitLocalX"]
        rechits_y = events["rechitLocalY"]
        rechits_r = events["rechitR"]
        rechits_phi = events["rechitPhi"]

        track_slope_x = events["trackSlopeX"]
        track_slope_y = events["trackSlopeY"]

        prophits_chamber = events["partialTrackChamber"]
        prophits_chi2 = events["partialTrackChi2"]
        prophits_slope_x = events["partialTrackSlopeX"]
        prophits_slope_y = events["partialTrackSlopeY"]
        prophits_eta = events["partialProphitEta"]
        prophits_x = events["partialProphitGlobalX"]
        prophits_y = events["partialProphitGlobalY"]

    """ Apply GE2/1 name conventions to chambers """
    rechits_chamber, rechits_module, rechits_eta = qc8_tools.to_layers(rechits_chamber, rechits_eta)
    prophits_chamber, prophits_module, prophits_eta = qc8_tools.to_layers(prophits_chamber, prophits_eta)

    list_chambers = np.unique(ak.flatten(rechits_chamber))
    list_modules = np.unique(ak.flatten(rechits_module))
    list_eta = np.unique(ak.values_astype(ak.flatten(rechits_eta), int))
    n_chambers = len(list_chambers)
    n_modules = len(list_modules)
    n_eta = len(list_eta)
    logging.info("{:2d} chambers in setup:       {}".format(n_chambers, list_chambers))
    logging.info("{:2d} modules in setup:        {}".format(n_modules, list_modules))
    logging.info("{:2d} eta partitions in setup: {}".format(n_eta, list_eta))

    """ Make some selections on the event quality """    
    track_quality_filter = (prophits_chi2<10)&(prophits_chi2>0.001)
    track_quality_filter = track_quality_filter&(abs(prophits_slope_x)<100e-3)
    track_quality_filter = track_quality_filter&(abs(prophits_slope_y)<0.01)
    prophits_chamber = prophits_chamber[track_quality_filter]
    prophits_chi2 = prophits_chi2[track_quality_filter]
    prophits_slope_x = prophits_slope_x[track_quality_filter]
    prophits_slope_y = prophits_slope_y[track_quality_filter]
    prophits_eta = prophits_eta[track_quality_filter]
    prophits_x = prophits_x[track_quality_filter]
    prophits_y = prophits_y[track_quality_filter]

    for ichamber,tested_chamber in enumerate(list_chambers):

        logging.info("Analyzing chamber {}...".format(tested_chamber))

        rechits_module_chamber = rechits_module[rechits_chamber==tested_chamber]
        rechits_eta_chamber = rechits_eta[rechits_chamber==tested_chamber]
        rechits_cluster_size_chamber = rechits_cluster_size[rechits_chamber==tested_chamber]
        rechits_x_chamber = rechits_x[rechits_chamber==tested_chamber]
        rechits_y_chamber = rechits_y[rechits_chamber==tested_chamber]
        rechits_r_chamber = rechits_r[rechits_chamber==tested_chamber]
        rechits_phi_chamber = rechits_phi[rechits_chamber==tested_chamber]

        prophits_module_chamber = prophits_module[prophits_chamber==tested_chamber]
        prophits_eta_chamber = prophits_eta[prophits_chamber==tested_chamber]
        prophits_x_chamber = prophits_x[prophits_chamber==tested_chamber]
        prophits_y_chamber = prophits_y[prophits_chamber==tested_chamber]

        residual_fig, residual_axs = plt.subplots(nrows=4, ncols=4, figsize=(11*4,9*4))
        residual_ofile = odir / "residuals/residuals_chamber{}.png".format(tested_chamber)
        residual_fig.suptitle("Detector {}".format(tested_chamber))

        if calculate_efficiency:

            matching_profile_fig, matching_profile_axs = plt.subplots(nrows=4, ncols=4, figsize=(11*4,9*4))
            matching_profile_ofile = odir / "matching-profiles/profile_chamber{}.png".format(tested_chamber)
            matching_profile_fig.suptitle("Detector {}".format(tested_chamber))

            efficiency_profile_fig, efficiency_profile_axs = plt.subplots(nrows=4, ncols=4, figsize=(11*4,9*4))
            efficiency_profile_ofile = odir / "efficiency-profiles/efficiency_chamber{}.png".format(tested_chamber)
            efficiency_profile_fig.suptitle("Detector {}".format(tested_chamber))

            efficiency_map_fig, efficiency_map_ax = plt.subplots(figsize=(11,11))
            efficiency_map_ofile = odir / "efficiency-maps/efficiency_chamber{}.png".format(tested_chamber)
            efficiency_map_fig.suptitle("Detector {}".format(tested_chamber))

            efficiency_values = list()
            efficiency_etas = list()
            efficiency_phis = [1,2,3]

        for ieta,tested_eta in enumerate(list_eta):

            # Add row for efficiency along this eta:
            efficiency_etas.append(tested_eta)

            filter_eta = rechits_eta_chamber==tested_eta
            rechits_module_eta = rechits_module_chamber[filter_eta]
            rechits_cluster_size_eta = rechits_cluster_size_chamber[filter_eta]
            rechits_x_eta = rechits_x_chamber[filter_eta]
            rechits_y_eta = rechits_y_chamber[filter_eta]
            rechits_r_eta = rechits_r_chamber[filter_eta]
            rechits_phi_eta = rechits_phi_chamber[filter_eta]

            prophits_x_eta = prophits_x_chamber[prophits_eta_chamber==tested_eta]
            prophits_y_eta = prophits_y_chamber[prophits_eta_chamber==tested_eta]

            module = np.unique(ak.flatten(rechits_module_eta))[0]
            irow = ieta % 4
            module_name = qc8_tools.MODULE_NAMES[module]
            logging.info("    Analyzing eta {} from module {}...".format(tested_eta, module_name))
            eta_title = "{} eta {}".format(module_name, tested_eta)

            #""" Plot cluster size profiles """
            #cluster_size_axs[module][irow].hist(
            #        ak.flatten(rechits_cluster_size_eta),
            #        bins=11, range=(0.5, 11.5),
            #        histtype="step", color="blue", linewidth=2
            #)
            #cluster_size_axs[module][irow].set_xlabel("Cluster size")
            #cluster_size_axs[module][irow].set_ylabel("Counts")
            #cluster_size_axs[module][irow].text(
            #    0.9, 0.9, f"Detector {tested_chamber} eta {tested_eta}", 
            #    transform=cluster_size_axs[module][irow].transAxes, ha="right"
            #)
            #cluster_size_axs[module][irow].set_title(eta_title)

            """ Process events for residuals """
            # Choose only events with both a prophit and a rechit and broadcast the prophits
            residual_filter = (ak.count(rechits_x_eta, axis=1)>0)&(ak.count(prophits_x_eta, axis=1)>0)
            rechits_x_broadcast, prophits_x_broadcast = ak.broadcast_arrays(
                    rechits_x_eta[residual_filter],
                    ak.flatten(prophits_x_eta[residual_filter])
            )
            residual_filter = (ak.count(rechits_y_eta, axis=1)>0)&(ak.count(prophits_y_eta, axis=1)>0)
            rechits_y_broadcast, prophits_y_broadcast = ak.broadcast_arrays(
                    rechits_y_eta[residual_filter],
                    ak.flatten(prophits_y_eta[residual_filter])
            )

            residuals_y = rechits_y_broadcast-prophits_y_broadcast
            residuals_x = rechits_x_broadcast-prophits_x_broadcast
            #residuals_x_flat = residuals_x[ak.argmin(residuals_x, axis=1, keepdims=True)].to_numpy()
            residuals_x_flat = ak.flatten(residuals_x).to_numpy()

            """ Plot and fit residuals """
            counts, x_edges, _ = residual_axs[module][irow].hist(
                residuals_x_flat,
                histtype="step", linewidth=2, color="blue",
                bins=100, range=(-5,5)
            )
            x_centers = 0.5*(x_edges[1:]+x_edges[:-1])
            residuals_binning = np.diff(x_edges).mean()
            residual_axs[module][irow].set_xlabel("All residual x (mm)")
            residual_axs[module][irow].set_ylabel("Events / {:1.0f} µm".format(residuals_binning*1e3))
            residual_axs[module][irow].set_title(eta_title)

            """ Calculate FWHM of residuals """
            residual_hm = common_tools.fwhm(x_centers, counts)
            residual_fwhm = residual_hm[1] - residual_hm[0]
            residual_axs[module][irow].text(
                    0.075, 0.9, "{:1.3f} FWHM mm".format(residual_fwhm),
                    transform = residual_axs[module][irow].transAxes, ha="left", va="top",
                    fontsize=28, linespacing=2.1,
                    )

            if calculate_efficiency:

                """ Calculate width of eta partition from geometry """
                chamber_id, eta_id = qc8_tools.to_chambers(tested_chamber, tested_eta)
                logging.debug("        Chamber {}, eta {} corresponds to module {}, eta {}".format(
                    tested_chamber, tested_eta, chamber_id, eta_id
                ))
                eta_width = setup.getDetector(chamber_id).getWidth(eta_id)
                logging.debug("        Eta partition has width {:1.2f}".format(eta_width))
                prophit_region = (-0.5*eta_width, 0.5*eta_width)
                prophit_bins = 21 # multiple of 3 to be able to later average by VFAT

                """ Plot numerator and denominator profiles for efficiency calculation """
                matching_cut = 5 # hardcoded cut for now. TODO: make user selectable
                logging.debug("        Applying matching cut {:1.2f} mm to residuals on chamber {}, eta {}".format(matching_cut, tested_chamber, tested_eta))
                event_has_match = ak.count_nonzero(abs(residuals_x)<matching_cut, axis=1)>0
                prophits_matching = prophits_x_eta[residual_filter][event_has_match]
                prophits_total = prophits_x_eta
                matching_counts, matching_edges, _ = matching_profile_axs[module][irow].hist(
                        ak.flatten(prophits_matching),
                        bins=prophit_bins, range=prophit_region,
                        histtype="step", label="Matching", color="red"
                        )
                total_counts, total_edges, _ = matching_profile_axs[module][irow].hist(
                        ak.flatten(prophits_total),
                        bins=prophit_bins, range=prophit_region,
                        histtype="step", label="Total", color="blue"
                        )
                matching_profile_axs[module][irow].set_xlabel("Prophit x (mm)")
                prophit_binning = (prophit_region[1]-prophit_region[0])/prophit_bins*1e3
                matching_profile_axs[module][irow].set_ylabel(f"Events / {prophit_binning:1.0f} µm")
                matching_profile_axs[module][irow].legend()
                matching_profile_axs[module][irow].set_title(eta_title)

                """ Plot efficiency profile """
                efficiency = np.where(total_counts==0, 0, matching_counts/total_counts)
                x_centers = 0.5*(matching_edges[1:]+matching_edges[:-1])
                x_errors = np.diff(matching_edges)/np.sqrt(12)
                efficiency_errors = np.sqrt(np.where(total_counts==0, 0, efficiency*(1-efficiency)/total_counts))
                average_efficiency = np.mean(efficiency)
                efficiency_profile_axs[module][irow].errorbar(
                        x_centers, efficiency,
                        xerr=x_errors, yerr=efficiency_errors,
                        fmt="ok"
                        )
                efficiency_profile_axs[module][irow].text(
                        0.9, 0.1,
                        "Average efficiency {:1.3f}".format(average_efficiency),
                        transform=efficiency_profile_axs[module][irow].transAxes,
                        ha="right"
                        )
                efficiency_profile_axs[module][irow].set_ylim(0., 1.05)
                efficiency_profile_axs[module][irow].set_xlabel("Propagated x (mm)")
                efficiency_profile_axs[module][irow].set_ylabel("Efficiency")
                efficiency_profile_axs[module][irow].set_title(eta_title)

                """ Divide in three bins to calculate efficiency per VFAT """
                n_matching = matching_counts.reshape(3,7).sum(axis=1)
                n_prophits = total_counts.reshape(3,7).sum(axis=1)
                efficiency_values.append(efficiency.reshape(3,7).mean(axis=1))

                """ Fill the efficiency dictionary """
                for phi in [1,2,3]:
                    efficiency_tuples.append(
                            (tested_chamber, tested_eta, phi, n_matching[phi-1], n_prophits[phi-1])
                            )

            #""" Plot correlations between propagated and residuals """
            #prophits_rechits_axs[module][irow].hist2d(
            #    ak.flatten(prophits_x_broadcast).to_numpy(), ak.flatten(rechits_x_broadcast).to_numpy(),
            #    bins=130, #range=(-200,200)
            #)
            #prophits_rechits_axs[module][irow].set_xlabel("Prophit x (mm)")
            #prophits_rechits_axs[module][irow].set_ylabel("Rechit x (mm)")
            #prophits_rechits_axs[module][irow].set_title(eta_title)

            #prophits_residual_axs[module][irow].hist2d(
            #    ak.flatten(prophits_x_broadcast).to_numpy(), ak.flatten(residuals_x).to_numpy(),
            #    bins=130, range=((-50,50),(-5,5))
            #)
            #prophits_residual_axs[module][irow].set_xlabel("Prophit x (mm)")
            #prophits_residual_axs[module][irow].set_ylabel("Residual x (mm)")
            #prophits_residual_axs[module][irow].set_title(eta_title)
            #prophits_residual_axs[module][irow].set_title(eta_title)

        residual_fig.tight_layout()
        logging.info("    Saving residual plot to {}".format(residual_ofile))
        residual_fig.savefig(residual_ofile)

        if calculate_efficiency:
            matching_profile_fig.tight_layout()
            logging.info("    Saving plots to {}".format(matching_profile_ofile))
            matching_profile_fig.savefig(matching_profile_ofile)

            efficiency_profile_fig.tight_layout()
            logging.info("    Saving plots to {}".format(efficiency_profile_ofile))
            efficiency_profile_fig.savefig(efficiency_profile_ofile)

            """ Plot efficiency map for entire detector """
            divider = make_axes_locatable(efficiency_map_ax)
            efficiency_cax = divider.append_axes("right", size="5%", pad=0.1)
            efficiency_im = efficiency_map_ax.imshow(
                    efficiency_values,
                    vmin=0., vmax=1.,
                    aspect="auto"
                    )
            efficiency_map_fig.colorbar(efficiency_im, cax=efficiency_cax, orientation="vertical")
            efficiency_map_ax.set_xticks(np.arange(len(efficiency_phis)), efficiency_phis)
            efficiency_map_ax.set_yticks(np.arange(len(efficiency_etas)), efficiency_etas)
            efficiency_map_ax.set_xlabel("phi")
            efficiency_map_ax.set_ylabel("eta")
            for map_i,map_eta in enumerate(efficiency_etas):
                for map_j,map_phi in enumerate(efficiency_phis):
                    efficiency_map_ax.text(
                            map_j, map_i,
                            "{:1.2f}".format(efficiency_values[map_i][map_j]),
                            ha="center", va="center", color="w", fontsize=20
                            )

            #efficiency_map_fig.tight_layout()
            logging.info("    Saving plots to {}".format(efficiency_map_ofile))
            efficiency_map_fig.savefig(efficiency_map_ofile)

    """ Save csv containing number of prophits and matching rechits per VFAT """
    if calculate_efficiency:
        efficiency_df = pd.DataFrame(
                efficiency_tuples,
                columns=["layer", "eta", "phi", "matching", "prophits"]
                ).astype(int)
        efficiency_summary_ofile = odir / "efficiency.csv"
        logging.info("Saving efficiency summary to {}".format(efficiency_summary_ofile))
        efficiency_df.to_csv(efficiency_summary_ofile, sep=";", index=False)

