import awkward as ak

MODULE_NAMES = [ "M1", "M2", "M3", "M4" ]

""" Convert module and eta partition name to GE2/1 conventions """
def to_layers(chamber, eta):
    layer = ak.values_astype(chamber / 4, int)
    module = ak.values_astype(chamber % 4, int)
    eta = ak.values_astype(eta + 4 * (3-module), int)
    return layer, module, eta

""" Inverse of to_layers: find detector ID and local eta from layer and global eta """
def to_chambers(layer, eta):
    chamber = 4*layer + int((16-eta)/4)
    eta = (eta-1) % 4 + 1
    return chamber, eta
