#include <cstdio>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdint>
#include <vector>
#include <array>
#include <utility>
#include <bitset>
#include <stdexcept>
#include <memory>
#include <signal.h>

#include <TFile.h>
#include <TTree.h>

#include <stdlib.h>
#include <zstd.h> 

#include "CLI11.hpp"
#include <fmt/core.h>

#include "DataFrame.h"
#include "mapping/vfat/ChamberMapping.h"
#include "mapping/vfat/StripMapping.h"
#include "RawGEMEvent.h"
#include "DigiEvent.h"

bool isInterrupted = false;
void interruptHandler(int dummy) {
    isInterrupted = true;
}
bool verbose{false};

class end_raw_file : public std::exception {};

class RawFileReader {

    public:

        RawFileReader(std::string ifile) {
            rawFile = std::fopen(ifile.c_str(), "rb");
            if (verbose) {
                fmt::print("Opened raw file {}.\n", ifile);
            }
            checkIsCompressed();
            // Prepare zstd context to decompress raw file
            if (m_isCompressed) {
                m_zstdBufferIn.size = ZSTD_DStreamInSize();
                m_zstdBufferIn.pos = ZSTD_DStreamInSize();
                m_zstdBufferInData = new char[m_zstdBufferIn.size];
                m_zstdBufferIn.src = m_zstdBufferInData;

                //m_zstdSizeOut = ZSTD_DStreamOutSize();
                m_zstdContext = ZSTD_createDCtx();
            }
        }

        /*~RawFileReader() {
            delete rawFile;
        }*/

        bool checkIsCompressed() {
            uint32_t magic;
            size_t magic_read = std::fread(&magic, 1, 4, rawFile);
            if (magic_read != 4) {
                throw std::invalid_argument(std::string("Error reading magic from first file: needed 4 bytes, but read ") + std::to_string(magic_read));
            }
            if (magic == ZSTD_MAGICNUMBER) {
                if (verbose) {
                    std::cout << "Input file is zstd compressed" << std::endl;
                }
                m_isCompressed = true;
            } else {
                if (verbose) {
                    std::cout << "Input file is not zstd compressed" << std::endl;
                }
                m_isCompressed = false;
            }
            rewind(rawFile);
            return m_isCompressed;
        }

        /**
         * @brief Read word from input file, using ZSTD decompression if necessary
         *
         * @param out buffer to store the data
         * @param size number of bytes to read
         * @param n number of words to read
         */
        size_t readFile(void *out, size_t size, size_t n, FILE *in) {
            if (m_isCompressed) {
                /*
                 * For compressed files run streaming decompression adapted from example:
                 * https://github.com/facebook/zstd/blob/dev/examples/streaming_decompression.c
                 */
                m_zstdBufferOut = { out, size, 0 };

                // Decompression stops when output buffer has same size as requested
                while (m_zstdBufferOut.pos < size) {

                    // If compressed buffer has been read, first read compressed buffer from file:
                    if (m_zstdBufferIn.pos == m_zstdBufferIn.size) {
                        size_t zstd_read = std::fread(m_zstdBufferInData, 1, m_zstdBufferIn.size, rawFile);

                        if (zstd_read != m_zstdBufferIn.size) {
                            // read less data than asked, check if there was an I/O error:
                            if (ferror(rawFile)) {
                                throw std::invalid_argument("Error reading from file");
                            } else {
                                // it means file has finished:
                                throw end_raw_file();
                            }
                        }
                        m_zstdBufferIn.pos = 0;
                        m_zstdBufferIn.size = zstd_read;
                    }

                    // Decompress the word in the input buffer:
                    size_t raw_read = ZSTD_decompressStream(m_zstdContext, &m_zstdBufferOut, &m_zstdBufferIn);
                    if (ZSTD_isError(raw_read)) {
                        throw std::invalid_argument(std::string("Error occured during ZSTD decompression: ") + ZSTD_getErrorName(raw_read));
                    }
                }
                return size;
            } else {
                return std::fread(out, n, size, rawFile);
            }
        }

        /** 
         * Read next event in raw file
         */
        AMCEvent *readEvent() {

            m_AMCEvent.clear();

            /**
             * Read the raw header
             * TODO: use the raw header (the first three words)
             */
            std::size_t sz = readFile(&m_word, sizeof(uint64_t), 1, rawFile);
            /**
             * End of file reached when the event is empty:
             */
            if (sz == 0) {
                m_AMCEvent.setEmpty(true);
                return &m_AMCEvent;
            }
            readFile(&m_word, sizeof(uint64_t), 1, rawFile);
            readFile(&m_word, sizeof(uint64_t), 1, rawFile);

            /**
             * Read the AMC event header and the GEM event header
             */
            readFile(&m_word, sizeof(uint64_t), 1, rawFile);
            m_AMCEvent.setAMCHeader1(m_word);
            readFile(&m_word, sizeof(uint64_t), 1, rawFile);
            m_AMCEvent.setAMCHeader2(m_word);
            readFile(&m_word, sizeof(uint64_t), 1, rawFile);
            m_AMCEvent.setGEMPayloadHeader(m_word);

            /**
             * Read the chamber payloads
             */
            for (uint8_t chamberIndex=0; chamberIndex<m_AMCEvent.chamberBlockCount; chamberIndex++) {

                std::shared_ptr<ChamberPayload> chamberPayload(new ChamberPayload());
                readFile(&m_word, sizeof(uint64_t), 1, rawFile);
                chamberPayload->setChamberHeader(m_word);

                int VFAT3Count = (int) (chamberPayload->VFAT3WordCount / 3);
                for (int VFAT3Index=0; VFAT3Index<VFAT3Count; VFAT3Index++) {
                    std::shared_ptr<VFAT3Payload> payload(new VFAT3Payload());

                    /**
                     * Read VFAT3 payload
                     */
                    readFile(&m_word, sizeof(uint64_t), 1, rawFile);
                    payload->setFirstWord(m_word);
                    readFile(&m_word, sizeof(uint64_t), 1, rawFile);
                    payload->setSecondWord(m_word);
                    readFile(&m_word, sizeof(uint64_t), 1, rawFile);
                    payload->setThirdWord(m_word);
                    chamberPayload->appendVFAT3Payload(std::move(payload));
                }

                readFile(&m_word, sizeof(uint64_t), 1, rawFile);
                chamberPayload->setChamberTrailer(m_word);

                m_AMCEvent.appendChamberPayload(std::move(chamberPayload));
            }
            readFile(&m_word, sizeof(uint64_t), 1, rawFile);
            m_AMCEvent.setGEMPayloadTrailer(m_word);
            readFile(&m_word, sizeof(uint64_t), 1, rawFile);
            m_AMCEvent.setAMCTrailer(m_word);
 
            return &m_AMCEvent;
        }

    private:
        
        /**
         * Binary file to read
         */
        std::FILE *rawFile;

        /**
         * Variables for zstd decompression
         */
        bool m_isCompressed;
        ZSTD_inBuffer m_zstdBufferIn;
        char *m_zstdBufferInData;
        size_t m_zstdSizeOut;
        ZSTD_outBuffer m_zstdBufferOut;
        ZSTD_DCtx* m_zstdContext;

        /** 
         * Raw event for single AMC
         */
        AMCEvent m_AMCEvent;

        /**
         * Buffer to store binary data read from file
         */
        uint64_t m_word;

        /**
         * Whether the event has size zero
         */
        bool m_isEmpty = false;

};

class DigiFileWriter {

    public:

        DigiFileWriter(std::string ofile, std::map<int, std::shared_ptr<StripMapping>> stripMappings, std::shared_ptr<ChamberMapping> chamberMapping) {
            m_stripMappings = stripMappings;
            m_chamberMapping = chamberMapping;

            digiFile = new TFile(ofile.c_str(), "RECREATE", "Digi file");
            digiTree = new TTree("outputtree", "outputtree");

            digiTree->Branch("orbitNumber", &m_digiEvent.orbitNumber);
            digiTree->Branch("bunchCounter", &m_digiEvent.bunchCounter);
            digiTree->Branch("eventCounter", &m_digiEvent.eventCounter);
            digiTree->Branch("runParameter", &m_digiEvent.runParameter);
            digiTree->Branch("pulse_stretch", &m_digiEvent.pulseStretch);
            digiTree->Branch("rawFed", &m_digiEvent.rawFED);
            digiTree->Branch("rawSlot", &m_digiEvent.rawSlot);
            digiTree->Branch("rawOH", &m_digiEvent.rawOH);
            digiTree->Branch("rawVFAT", &m_digiEvent.rawVFAT);
            digiTree->Branch("rawChannel", &m_digiEvent.rawChannel);

            // digi variable branches
            digiTree->Branch("digiBX", &m_digiEvent.subEventCount);
            digiTree->Branch("digiStripChamber", &m_digiEvent.digiStripChamber);
            digiTree->Branch("digiStripEta", &m_digiEvent.digiStripEta);
            digiTree->Branch("digiStrip", &m_digiEvent.digiStrip);

            /**
             * Unused by this unpacker for now, but we need the branches
             * for compatibility with SRS data:
             */
            digiTree->Branch("digiStripCharge", &m_digiEvent.digiStripCharge);
            digiTree->Branch("digiStripTime", &m_digiEvent.digiStripTime);
            digiTree->Branch("digiPadChamber", &m_digiEvent.digiPadChamber);
            digiTree->Branch("digiPadX", &m_digiEvent.digiPadX);
            digiTree->Branch("digiPadY", &m_digiEvent.digiPadY);
            digiTree->Branch("digiPadCharge", &m_digiEvent.digiPadCharge);
            digiTree->Branch("digiPadTime", &m_digiEvent.digiPadTime);

        }

        ~DigiFileWriter() {
            digiTree->Write();
            digiFile->Close();
        }

        /**
         * Convert raw GEM event to digi event
         * by applying mapping and decoding VFAT payload
         */
        DigiEvent *fromRaw(RawGEMEvent *event) {
            /**
             * Fill scalar variables
             */
            m_digiEvent.runParameter = event->runParameters;
            m_digiEvent.pulseStretch = event->pulseStretch;
            m_digiEvent.orbitNumber = event->orbitCounter;
            m_digiEvent.bunchCounter = event->bunchCounter;
            m_digiEvent.eventCounter = event->eventCounter;

            /**
             * To fill hit variables, iterate on FED, slot, OH and VFAT
             */
            for (auto & amc_event : *event->getAMCEvents()) {
                for (auto & chamber_payload : *amc_event->getChamberPayloads()) {
                    for (auto & vfat_payload : *chamber_payload->getVFAT3Payloads()) {
                        /**
                         * Retrieve chamber mapping
                         */
                        ChamberMappingKey p{amc_event->FED, amc_event->slot, chamber_payload->OH, vfat_payload->position};
                        chamber = m_chamberMapping->getChamber(p);
                        if (m_stripMappings.count(chamber) == 0) {
                            throw std::out_of_range(fmt::format("Chamber {} does not have a strip mapping", chamber));
                        }
                        auto stripMapping = m_stripMappings.at(chamber);

                        /**
                         * Map VFAT3 channels to strip
                         */
                        for (auto hitChannel : *vfat_payload->getHits()) {

                            /**
                             * Assign raw hit variables
                             */
                            m_digiEvent.rawFED->push_back(amc_event->FED);
                            m_digiEvent.rawSlot->push_back(amc_event->slot);
                            m_digiEvent.rawOH->push_back(chamber_payload->OH);
                            m_digiEvent.rawVFAT->push_back(vfat_payload->position);
                            m_digiEvent.rawChannel->push_back(hitChannel);

                            /**
                             * Assign mapped hit variables
                             */
                            VFATChannelTuple t{vfat_payload->position, hitChannel};
                            //std::vector<int> *subEventCount; TODO: add for multi-BX readout
                            m_digiEvent.digiStripChamber->push_back(chamber);
                            m_digiEvent.digiStripEta->push_back(stripMapping->getEta(t));
                            m_digiEvent.digiStrip->push_back(stripMapping->getStrip(t));
                            m_digiEvent.digiStripCharge->push_back(1);
                            m_digiEvent.digiStripTime->push_back(m_digiEvent.subEventCount);
                        }
                    }
                }
            }

            return &m_digiEvent;
        }

        void writeEvent() {
            digiTree->Fill();
        }

    private:

        std::shared_ptr<ChamberMapping> m_chamberMapping;
        std::map<int, std::shared_ptr<StripMapping>> m_stripMappings;

        TFile *digiFile;
        TTree *digiTree;

        DigiEvent m_digiEvent;

        int chamber;

};

int main (int argc, char** argv) {

    std::vector<std::string> raw_filenames;
    std::string digi_filename;
    uint64_t max_events{0}, every{0};
    std::string geometry;
    bool permissive{false};

    CLI::App app{"Convert VFAT GEM raw files to digi"};
    app.add_option("--input", raw_filenames, "Raw input file paths")->required();
    app.add_option("--output", digi_filename, "Digi output file path")->required();
    app.add_option("--geometry", geometry, "Geometry name");
    app.add_option("-n,--events", max_events, "Maximum number of events to read");
    app.add_option("--every", every, "Number of events to skip after each event read");
    app.add_flag("--permissive", permissive, "Continue even when counters in corresponding events in different input files mismatch");
    app.add_flag("-v,--verbose", verbose, "Verbose mode");
    try {
        app.parse(argc, argv);
    } catch (const CLI::ParseError &e) {
        return app.exit(e);
    }

    std::cout << "Raw input files: ";
    for (auto f:raw_filenames) std::cout << f << " ";
    std::cout << std::endl;
    std::cout << "Digi output file: " << digi_filename << std::endl;

    std::cout << "Reading mapping files for geometry " << geometry << "..." << std::endl;
    std::string mappingBaseDir = std::string(std::getenv("ANALYSIS_HOME"))+"mapping/"+geometry;
    std::map<int, std::shared_ptr<StripMapping>> stripMappings;
    std::shared_ptr<ChamberMapping> chamberMapping(new ChamberMapping(mappingBaseDir+"/mapping.csv"));
    chamberMapping->print();

    if (geometry=="oct2021" || geometry=="may2022") {
        auto trackerStripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/tracker_mapping.csv"));
        auto ge21StripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/ge21_mapping.csv"));
        auto me0StripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/me0_mapping.csv"));
        std::cout << "Mapping files ok." << std::endl;

        stripMappings = {
            {0, trackerStripMapping},
            {1, trackerStripMapping},
            {2, trackerStripMapping},
            {3, trackerStripMapping},
            {4, trackerStripMapping},
            {5, trackerStripMapping},
            {6, trackerStripMapping},
            {7, trackerStripMapping},
            {8, ge21StripMapping},
            {9, me0StripMapping},
            {10, me0StripMapping},
        };
    } else if (geometry=="july2022") {
        auto trackerStripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/tracker_mapping.csv"));
        auto me0StripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/me0_mapping.csv"));
        std::cout << "Mapping files ok." << std::endl;

        stripMappings = {
            {0, trackerStripMapping},
            {1, trackerStripMapping},
            {2, trackerStripMapping},
            {3, trackerStripMapping},
            {4, trackerStripMapping},
            {5, trackerStripMapping},
            {6, me0StripMapping},
        };
    } else if (geometry=="me0stack") {
        auto me0StripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/me0.csv"));
        std::cout << "Mapping files ok." << std::endl;

        stripMappings = {
            {0, me0StripMapping},
            {1, me0StripMapping},
            {2, me0StripMapping},
        };
    } else if (geometry=="stack-tb-april2023") {
        auto me0StripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/me0.csv"));
        std::cout << "Mapping files ok." << std::endl;

        stripMappings = {
            {0, me0StripMapping},
            {1, me0StripMapping},
            {2, me0StripMapping},
            {3, me0StripMapping},
        };
    } else if (geometry=="qc8") {
        auto ge21StripMapping = std::shared_ptr<StripMapping>(new StripMapping(mappingBaseDir+"/ge21module.csv"));
        ge21StripMapping->print();
        std::cout << "Mapping files ok." << std::endl;
        stripMappings = {
            {0, ge21StripMapping},
            {1, ge21StripMapping},
            {2, ge21StripMapping},
            {3, ge21StripMapping},
            {4, ge21StripMapping},
            {5, ge21StripMapping},
            {6, ge21StripMapping},
            {7, ge21StripMapping},
            {8, ge21StripMapping},
            {9, ge21StripMapping},
            {10, ge21StripMapping},
            {11, ge21StripMapping},
            {12, ge21StripMapping},
            {13, ge21StripMapping},
            {14, ge21StripMapping},
            {15, ge21StripMapping},
            {16, ge21StripMapping},
            {17, ge21StripMapping},
            {18, ge21StripMapping},
            {19, ge21StripMapping},
            {20, ge21StripMapping},
            {21, ge21StripMapping},
            {22, ge21StripMapping},
            {23, ge21StripMapping}
        };
    }
    else {
        std::cout << "Error: geometry " << geometry << " not supported yet." << std::endl;
        return -1;
    }

    // if a channel ask file exists, use it:
    DataFrame maskDataFrame;
    std::string maskCsvPath(std::string(std::getenv("ANALYSIS_HOME"))+"masks/"+geometry+".csv");
    std::ifstream maskFile(maskCsvPath);
    if (maskFile.good()) {
        std::cout << "Using masking file " << maskCsvPath << std::endl;
        maskDataFrame = DataFrame::fromCsv(maskCsvPath, ";");
        //maskDataFrame.print();
    } else {
        std::cout << "No channel masking file found." << std::endl;
    }

    std::vector<RawFileReader> readers;
    for (auto ifile:raw_filenames) {
        readers.push_back(RawFileReader{ifile});
    }
    DigiFileWriter writer{digi_filename, stripMappings, chamberMapping};
    RawGEMEvent *rawEvent;
    DigiEvent *digiEvent;

    if (max_events>0) {
        std::cout << "Processing " << max_events << " events..." << std::endl; 
    } else {
        std::cout << "Processing all events..." << std::endl; 
    }

    /**
     * Store event counters to decide whether to group them,
     * in case this is a fake multi-BX run
     */
    int previousEventCounter;
    bool isMegaEventBad{false};
    bool hasRunEnded{false};
    /**
     * Loop on raw events
     */
    signal(SIGINT, interruptHandler);
    for (int eventIndex=0; !isInterrupted; eventIndex++) {

        if ((max_events>0) && (eventIndex>=max_events)) {
            break;
        }
        if (verbose) {
            std::cout << "Event " << eventIndex << std::endl;
        } else {
            if (eventIndex % 1000 == 0) {
                std::cout << "\rReading event " << eventIndex;
                std::cout.flush();
            }
        }

        /**
         * Read AMC events from all raw files and put them together
         * in a single raw event.
         */
        rawEvent = new RawGEMEvent();
        for (int i=0; i<readers.size(); i++) {
            /**
             * The run has ended whenever you meet the first empty event,
             * or if while reading the event we get no data
             */
            if (rawEvent->isEmpty()) {
                hasRunEnded = true;
            } else {
                try {
                    rawEvent->append(std::unique_ptr<AMCEvent>(readers.at(i).readEvent()), permissive);
                } catch (end_raw_file &e) {
                    fmt::print("\nMet end of run while reading file {}", i);
                    hasRunEnded = true;
                }
            }
        }
        if (hasRunEnded) {
            break;
        }
        /**
         * Convert the event to digi,
         * then possibly print it 
         */
        digiEvent = writer.fromRaw(rawEvent);
        if (verbose) {
            rawEvent->print();
            digiEvent->print();
        }
        
        /**
         * In case of fake multi-BX readout,
         * check that the ECs are consecutive for the fake events
         */
        if (rawEvent->fakeL1A) {
            /** If two fake events are not consecutive, 
             * it means we dropped one.
             * Flag the mega-event as bad so it will be skipped
             */
            if (rawEvent->eventCounter != previousEventCounter+1) {
                isMegaEventBad = true;
                fmt::print("Event {} has non-consecutive ECs between {} and {}, will skip mega-event...",
                        eventIndex, previousEventCounter, rawEvent->eventCounter
                        );
            }
        } else {
            /**
             * This is the start of a new mega-event,
             * so we can reset the "bad mega-event" flag
             */
            isMegaEventBad = false;
        }
        /**
         * If the mega-event has been marked as bad
         * (either now or in a previous sub-event),
         * still read the sub-event but empty it afterwards
         */
        if (isMegaEventBad) {
            digiEvent->clear();
            fmt::print("Skipping event {} with EC {} because it belongs to a bad mega-event...", eventIndex, rawEvent->eventCounter);
        }

        /**
         * If not multi-BX run: save to tree and reset all branch variables;
         * if multi-BX run, check that we reached the end of mega-event, then save,
         * otherwise just increase sub-event counter
         */
        if (digiEvent->subEventCount == rawEvent->fakeMultiBX) {
            /**
             * Save to tree every n events
             */
            if (every == 0 || eventIndex % every == 0) {
                writer.writeEvent();
            }
            digiEvent->clear();
        } else {
            digiEvent->subEventCount++;
        }
        previousEventCounter = rawEvent->eventCounter;

        //delete rawEvent;
    }

    std::cout << std::endl;
    std::cout << "Output file saved to " << digi_filename << std::endl;
}
