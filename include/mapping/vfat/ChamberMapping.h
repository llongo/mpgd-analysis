#include <cstdio>
#include <string>
#include <tuple>
#include <unordered_map>

#include "mapping/Mapping.h"

#ifndef DEF_VFAT_CHAMBER_MAPPING
#define DEF_VFAT_CHAMBER_MAPPING

/**
 * Using std::unordered_map instead of std::map makes the unpacker about 2x faster.
 * Since std::tuple is not specialized for hashing,
 * it cannot be used as key with std::unordered_map.
 * Hashing implemented as documented here:
 * https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n3876.pdf
 */
typedef std::tuple<int, int, int, int> ChamberMappingKey;
template <class T> inline void hash_combine(std::size_t & s, const T & v) {
    std::hash<T> h;
    s^= h(v) + 0x9e3779b9 + (s<< 6) + (s>> 2);
}
struct key_hash : public std::unary_function<ChamberMappingKey, std::size_t> {
    std::size_t operator()(const ChamberMappingKey& k) const {
        std::size_t h = 0;
        hash_combine(h, std::get<0>(k));
        hash_combine(h, std::get<1>(k));
        hash_combine(h, std::get<2>(k));
        hash_combine(h, std::get<3>(k));
        return h;
    }
};

class ChamberMapping : public Mapping {
    
    public:
        
        /*
         * Default constructor
         */
        ChamberMapping() {}

        /**
         * Copy constructor
         */
        ChamberMapping(ChamberMapping *otherMapping) : ChamberMapping(otherMapping->getMappingFilePath()) {}

        /**
         * Constructor from mapping CSV file
         */
	    ChamberMapping(std::string mappingFile);
	    
        /**
         * Getter methods
         */
        int getChamber(ChamberMappingKey p) { return toChamber[p]; }

        void print();

    protected:

        std::unordered_map<ChamberMappingKey, int, key_hash> toChamber;
};

#endif
