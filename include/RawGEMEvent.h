#include <fmt/core.h>
#include <fmt/ranges.h>

#ifndef DEF_RAW_GEM_EVENT
#define DEF_RAW_GEM_EVENT

/**
 * Implements v302a CMS GEM data format;
 * see https://docs.google.com/spreadsheets/d/1iKMl68vMsSWgr8ekVdsJYTk7-Pgy8paxq98xj6GtoVo
 * note: the spreadsheet does not contain the specifications for the fake multi-BX readout
 */

class empty_event_error : public std::exception {};
class event_sync_error : public std::exception {};

class VFAT3Payload {

    public:

        VFAT3Payload() {}

        void clear() {
            m_hitChannels.clear();
        }

        std::vector<int> *getHits() {
            return &m_hitChannels;
        }

        void setFirstWord(uint64_t word) {
            position = 0b11111 & (word >> 56);
            CRCCheck = 0b1 & (word >> 48);
            header = word >> 40;
            eventCounter = word >> 32;
            bunchCounter = word >> 16;
            lastChannelData = 0xffff000000000000 & (word << 48);
        }

        void setSecondWord(uint64_t word) {
            lastChannelData = lastChannelData | (0x0000ffffffffffff & word >> 16);
            firstChannelData = 0xffff000000000000 & (word << 48);
        }

        void setThirdWord(uint64_t word) {
            firstChannelData = firstChannelData | (0x0000ffffffffffff & word >> 16);
            CRC = 0xffff & word;

            /**
             * Fill array of channel with hit from the two masks
             */
            for (int i=0; i<64; i++) {
                if (firstChannelData & (1LL << i)) {
                    m_hitChannels.push_back(i);
                }
                if (lastChannelData & (1LL << i)) {
                    m_hitChannels.push_back(i+64);
                }
            }
        }

        /**
         * First VFAT3 word
         */
        uint8_t position;
        uint8_t CRCCheck;
        uint8_t header;
        uint8_t eventCounter;
        uint16_t bunchCounter;

        /**
         * Channel data in first, second and third words
         */
        uint64_t lastChannelData;
        uint64_t firstChannelData;

        /**
         * Third VFAT word
         */
        uint16_t CRC;

    private:

        /**
         * List of channels containing a hit:
         */
        std::vector<int> m_hitChannels;
};

class ChamberPayload {

    public:

        ChamberPayload() {}

        ~ChamberPayload() {
            clear();
        }

        void clear() {
            m_VFAT3Payloads.clear();
        }

        void appendVFAT3Payload(std::shared_ptr<VFAT3Payload> payload) {
            m_VFAT3Payloads.push_back(payload);
        }

        std::vector<std::shared_ptr<VFAT3Payload>> *getVFAT3Payloads() {
            return &m_VFAT3Payloads;
        }

        void setChamberHeader(uint64_t word) {
            calibrationChannelNumber = 0b1111111 & (word >> 40);
            OH = 0b11111 & (word >> 35);
            VFAT3WordCount = 0xff & (word >> 23);
            flagMask = 0b1111111111111 & (word >> 10);
            for (int iflag=0; iflag<13; iflag++) {
                headerFlags[iflag] = 0b1 & (flagMask >> (iflag));
            }
        }

        void setChamberTrailer(uint64_t word) {
            inputFIFOUnderflow = 0b1 & (word >> 51);
            zeroSuppressedMask = 0xffff & (word >> 24);
            VFATMask = 0xffff & (word >> 24);
        }

        /**
         * Chamber header words
         */
        uint8_t calibrationChannelNumber;
        uint8_t OH;
        uint16_t VFAT3WordCount;
        /* Flags about integrity of event: */
        uint16_t flagMask;
        uint8_t headerFlags[13];

        /**
         * Chamber trailer words
         */
        uint8_t inputFIFOUnderflow;
        uint32_t zeroSuppressedMask;
        uint32_t VFATMask;

    private:

        std::vector<std::shared_ptr<VFAT3Payload>> m_VFAT3Payloads;

};

class AMCEvent {

    public:

        AMCEvent() {}

        ~AMCEvent() {
            clear();
        }

        void clear() {
            m_chamberPayloads.clear();
            m_isEmpty = false;
        }

        void appendChamberPayload(std::shared_ptr<ChamberPayload> && payload) {
            m_chamberPayloads.push_back(payload);
        }

        std::vector<std::shared_ptr<ChamberPayload>> *getChamberPayloads() {
            return &m_chamberPayloads;
        }

        bool isEmpty() {
            return m_isEmpty;
        }

        void setEmpty(bool isEmpty) {
            m_isEmpty = isEmpty;
        }

        void setAMCHeader1(uint64_t word) {
            slot = 0x0f & (word >> 56);
            eventCounter = 0x00ffffff & (word >> 32);
            bunchCounter = 0x0fff & (word >> 20);
            eventLengthHeader = 0xfffff & word;
        }

        void setAMCHeader2(uint64_t word) {
            formatVersion = 0x0f & (word >> 60);
            orbitCounter = word >> 16;
            softSlot= 0xf & (word >> 12);
            FED = 0xfff & word;
        }

        void setGEMPayloadHeader(uint64_t word) {
            chamberMask = 0xffffff & (word >> 40);
            fakeMultiBX = 0xf & (word >> 16);
            chamberBlockCount = 0b11111 & (word >> 11);
            GEMPayloadFormat = 0b111 & (word >> 8);
            VFATPayloadType = 0xf & (word >> 4);
            TTSState = 0xf & word;
        }

        void setGEMPayloadTrailer(uint64_t word) {
            linkTimeout = 0xffffff & (word >> 40);
            runType = 0xf & (word >> 32);
            runParameters = 0x03ff & (word >> 8); 
            pulseStretch = (int) (0x07 & (word >> 18));
            fakeL1A = 0x1 & (word >> 2);
        }

        void setAMCTrailer(uint64_t word) {
            eventCRC = word >> 32;
            eventLengthTrailer = 0xfffff & word;
        }

        /**
         * Variables in first AMC header
         */
        uint8_t slot;        
        uint32_t eventCounter;          
        uint16_t bunchCounter;    
        uint32_t eventLengthHeader;

        /**
         * Variables in second AMC header
         */
        uint8_t formatVersion;
        uint32_t orbitCounter;
        /* Identical to slot number: */
        uint8_t softSlot;
        uint16_t FED;

        /**
         * Variables in GEM payload
         */
        /* Mask containing which chambers have sent data in the event: */
        uint32_t chamberMask; 
        /* Number of fake multi-BX L1As per real L1A: */
        uint8_t fakeMultiBX;
        /* Number of chambers that have sent data in the event: */
        uint8_t chamberBlockCount;
        /* Version number of the GEM payload data format: */
        uint8_t GEMPayloadFormat;
        uint8_t VFATPayloadType;
        uint8_t TTSState;

        /**
         * Variables in GEM trailer
         */
        uint32_t linkTimeout;     
        uint8_t runType;
        /* The actual run parameter is 24-bit long, but we use only the length for the latency: */
        uint16_t runParameters;
        /* The pulse stretch takes a piece of the run parameter word: */
        uint8_t pulseStretch;
        /* In a multi-BX run, determines if this is a real or fake L1A: */
        bool fakeL1A;

        /**
         * Variables in AMC trailer
         */
        uint32_t eventCRC;
        uint32_t eventLengthTrailer;

    private:

        bool m_isEmpty{false};

        std::vector<std::shared_ptr<ChamberPayload>> m_chamberPayloads;

};

class RawGEMEvent {
    
    public:

        /**
         * Empty constructor
         */
        RawGEMEvent() {}

        ~RawGEMEvent() {
            clear();
        }

        std::vector<std::unique_ptr<AMCEvent>> *getAMCEvents() {
            return &m_AMCEvents;
        }

        void clear() {
            m_AMCEvents.clear();
        }

        /**
         * @brief Add smart pointer to an AMCEvent to the raw event
         * @param event the AMCEvent to be added
         * Returns std::logic_error if any counters with the other AMCEvents do not match
         * and empty_event_error if the AMCEvent has length 0 bytes
         */
        void append(std::unique_ptr<AMCEvent> event, bool permissive=false) {
            if (event->isEmpty()) {
                fmt::print("\nAMC event number {} is empty\n", m_AMCEvents.size());
            }
            if (m_AMCEvents.size() == 0) {
                /**
                 * If it is the first AMC event, assign the event variables to the entire raw event
                 */
                eventCounter = event->eventCounter;
                bunchCounter = event->bunchCounter;
                orbitCounter = event->orbitCounter;
                fakeMultiBX = event->fakeMultiBX;
                fakeL1A = event->fakeL1A;
                runParameters = event->runParameters;
                pulseStretch = event->pulseStretch;
            } else if (!permissive) {
                /**
                 * Compare the common variables and throw an exception if they are not the same
                 */
                synchronized &= eventCounter == event->eventCounter;
                synchronized &= bunchCounter == event->bunchCounter;
                synchronized &= orbitCounter == event->orbitCounter;
                synchronized &= fakeMultiBX == event->fakeMultiBX;
                synchronized &= fakeL1A == event->fakeL1A;
                synchronized &= runParameters == event->runParameters;
                synchronized &= pulseStretch == event->pulseStretch;
                if (!synchronized) {
                    throw event_sync_error();
                }
            }
            m_AMCEvents.push_back(std::move(event));
        }

        void print() {
            std::cout << "Raw event:" << std::endl;
            for (auto & amc_event : m_AMCEvents) {
                fmt::print("    FED: {}\n", amc_event->FED);
                fmt::print("    Slot: {}\n", amc_event->slot);
                fmt::print("    Orbit counter: {}\n", amc_event->orbitCounter);
                fmt::print("    Bunch counter: {}\n", amc_event->bunchCounter);
                fmt::print("    Event counter: {}\n", amc_event->eventCounter);
                fmt::print("    Chamber mask: {:b}\n", amc_event->chamberMask);
                fmt::print("    Number of chamber blocks: {}\n", amc_event->chamberBlockCount);
                fmt::print("    Fake multi-BX number: {}\n", amc_event->fakeMultiBX);
                fmt::print("    TTS state: {:#x}\n", amc_event->TTSState);
                fmt::print("    Link timeout: {:#x}\n", amc_event->linkTimeout);
                fmt::print("    Run type: {:#x}\n", amc_event->runType);
                fmt::print("    Run parameter (latency): {}\n", amc_event->runParameters);
                fmt::print("    Pulse stretch: {}\n", amc_event->pulseStretch);
                fmt::print("    Is fake L1A: {}\n", amc_event->fakeL1A);
                fmt::print("    Event CRC: {:#x}\n", amc_event->eventCRC);

                for (auto & chamber_payload : *amc_event->getChamberPayloads()) {
                    fmt::print("        OH {}\n", chamber_payload->OH);
                    fmt::print("        Number of VFAT words: {}\n", chamber_payload->VFAT3WordCount);
                    fmt::print("        Flags {:013b}{:1b}\n", chamber_payload->flagMask, chamber_payload->inputFIFOUnderflow);
                    fmt::print("        Zero suppressed mask: {:#b}\n", chamber_payload->zeroSuppressedMask);
                    fmt::print("        VFAT mask: {:#b}\n", chamber_payload->VFATMask);

                    for (auto & vfat_payload: *chamber_payload->getVFAT3Payloads()) {
                        fmt::print("            VFAT {:2d} | ", vfat_payload->position); 
                        fmt::print("CRC check: {}, ", vfat_payload->CRCCheck); 
                        fmt::print("BC: {}, ", vfat_payload->bunchCounter); 
                        fmt::print("EC: {}, ", vfat_payload->eventCounter); 
                        fmt::print("CRC: {:16b}, ", vfat_payload->CRC); 
                        //fmt::print("Channel data: {:b} {:b}\n", vfat_payload->firstChannelData, vfat_payload->lastChannelData); 
                        fmt::print("channels hit: {}\n", *vfat_payload->getHits()); 
                    }
                }
            }
        }

        /** 
         * @brief Determine if at least one AMC event in the raw event is empty
         * GEM event is empty if at least one AMC event in it is empty
         */
        bool isEmpty() {
            bool answer = false;
            for (int i=0; i<m_AMCEvents.size(); i++) {
                answer = answer && m_AMCEvents.at(i)->isEmpty();
            }
            return answer;
        }

        /**
         * Variables that should be identical in all AMC events.
         * they are checked when a new AMC event is appended;
         * if it is not, an exception is thrown
         */
        uint32_t eventCounter;
        uint16_t bunchCounter;
        uint32_t orbitCounter;
        uint8_t fakeMultiBX;
        bool fakeL1A;
        uint16_t runParameters;
        uint8_t pulseStretch;

    private:

        std::vector<std::unique_ptr<AMCEvent>> m_AMCEvents;

        /**
         * Flag to check whether the AMC events have identical common variables
         */
        bool synchronized{true};

};

#endif
