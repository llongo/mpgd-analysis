#include <iostream>
#include <math.h>

#include "Digi.h"

#ifndef DEF_DIGI_PAD
#define DEF_DIGI_PAD

class DigiPad : public Digi {
    
    public:

        /**
         * Copy constructor
         */
        DigiPad(DigiPad *digi) : Digi(digi->getChamber(), digi->getCharge(), digi->getTime()), m_padX{digi->getPadX()}, m_padY{digi->getPadY()} {}

        /**
         * Constructor from chamber, pad x, pad y
         */
        DigiPad(int chamber, int padX, int padY) : Digi(chamber), m_padX{padX}, m_padY{padY} {}
        DigiPad(int chamber, int padX, int padY, int charge, int time) : Digi(chamber, charge, time), m_padX{padX}, m_padY{padY} {}

        int getPadX() { return m_padX; }
        int getPadY() { return m_padY; }

        /**
         * Return true if two digis are consecutive
         * either in x or in y (i.e. horizontally or vertically),
         * but not in both (i.e. not diagonally).
         *
         * @param digi to be compared with 
         */
        bool isNeighbour(DigiPad otherDigi) {
            return pow(m_padX-otherDigi.getPadX(),2) + pow(m_padY-otherDigi.getPadY(),2)==1;
        }

        std::ostream& operator<<(std::ostream &os) {
            return os << "chamber " << m_chamber << " pad x " << m_padX << " pad y " << m_padY;
        }

    private:
        int m_padX, m_padY;
};

#endif
