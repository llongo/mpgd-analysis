#include <cstdio>
#include <vector>

#ifndef DEF_DETECTOR_LAYER
#define DEF_DETECTOR_LAYER

class DetectorLayer {

    public:

        DetectorLayer(double);
        DetectorLayer(double, std::vector<int>);

        void print();
        std::vector<int> getChambers();
        void addChamber(int);
        void setTracking(bool);
        bool isTracking();
        double getPositionZ();

    private:

        std::vector<int> m_chambers;
        bool m_isTracking = false;
        double m_z;
};

#endif
