#include <cstdio>
#include <string>

#ifndef DEF_READOUT_MAPPING_APV
#define DEF_READOUT_MAPPING_APV

typedef std::tuple<int, int, int> FedChipChannelTuple;

class ReadoutMappingAPV {
    
    public:

        /**
         * Default constructor
         */
        ReadoutMappingAPV() {}

        /**
         * Copy constructor
         */
        ReadoutMappingAPV(ReadoutMappingAPV *otherMapping) : ReadoutMappingAPV(otherMapping->getMappingFilePath()) {}

        /**
         * Constructor from mapping CSV file
         */
	    ReadoutMappingAPV(std::string mappingFilePath);

        std::string getMappingFilePath() { return m_mappingFilePath; }

	    int read();
	    void print();

        /**
         * Getter methods
         */
        int getChamber(FedChipChannelTuple p) { return toChamber[p]; }
        int getType(FedChipChannelTuple p) { return toType[p]; }
        int getPadX(FedChipChannelTuple p) { return toPadX[p]; }
        int getPadY(FedChipChannelTuple p) { return toPadY[p]; }
        int getStrip(FedChipChannelTuple p) { return toStrip[p]; }

    protected:

        std::map<FedChipChannelTuple, int> toChamber;
        std::map<FedChipChannelTuple, int> toType;
        std::map<FedChipChannelTuple, int> toPadX;
        std::map<FedChipChannelTuple, int> toPadY;
        std::map<FedChipChannelTuple, int> toStrip;

        std::string m_mappingFilePath;
};

#endif
