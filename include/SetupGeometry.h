#include <cstdio>
#include <vector>

#include "DataFrame.h"
#include "DetectorGeometry.h"
#include "DetectorLayer.h"

#ifndef DEF_SETUP_GEOMETRY
#define DEF_SETUP_GEOMETRY

class SetupGeometry {

    public:

        SetupGeometry(std::string geometryFile);

        void print();
        DetectorGeometry *getChamber(int chamber);
        int getLayer(int chamber);
        bool isTrackingChamber(int chamber);
        bool isTrackingLayer(int layer);

        /* List of tracking chamber IDs */
        std::vector<int> trackerChambers;
        /* Map from chamber number to detector */
        std::map<int, DetectorGeometry> detectorMap;
        /* List of detectors with the same z position */
        std::map<double, std::vector<int>> layerMap;
        /* Map each detector to corresponding layer */
        std::map<int, int> detectorToLayerMap;
        /* List of layer objects */
        std::vector<DetectorLayer> layers;

    private:

        DataFrame geometryDataFrame;
};

#endif
