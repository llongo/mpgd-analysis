#ifndef DEF_DETECTORGEOMETRY
#define DEF_DETECTORGEOMETRY

#include "ClusterStrip.h"
#include "ClusterPad.h"
#include "Rechit.h"

#define INVERSE_SQRT_12 0.2886751345948129

class DetectorGeometry {

    public:

        DetectorGeometry(int chamber, double baseNarrow, double baseWide, double height, int nEta, std::string readoutType, int nReadoutElements);
        DetectorGeometry(int chamber, double baseNarrow, double baseWide, double height, int nEta, std::string readoutType, int nReadoutElements, double x, double y, double z, double theta);

        /**
         * Default empty constructor
         */
        DetectorGeometry() {};

        /** 
         * Copy constructor
         */
        DetectorGeometry(const DetectorGeometry&);
        
        /**
         * Create rechit in local detector coordinates
         *
         * @param cluster to create rechit from
         * @return rechit mapped to local detector coordinates
         */
        Rechit createRechit(ClusterStrip cluster) const;
        Rechit createRechit(ClusterPad cluster) const;

        /**
         * Map already existing rechit to global detector geometry
         *
         * @param rechit to map
         */
        void mapRechit(Rechit *rechit);

        void setPosition(double x, double y, double z) {
            m_position[0] = x;
            m_position[1] = y;
            m_position[2] = z;
        }
        void setPosition(double x, double y, double z, double theta) {
            setPosition(x, y, z);
            m_theta = theta;
        }

        /**
         * Verbosely print detector geometry and position
         */
        void print() const;

        double getPositionX() const { return m_position[0]; }
        double getPositionY() const { return m_position[1]; }
        double getPositionZ() const { return m_position[2]; }
        double getTheta() const { return m_theta; }
        double getArea() const { return m_area; }
        double getOriginY() const { return m_originY; }
        double getAperture() const { return m_aperture; }

        double getNEta() const { return m_numberPartitions; }
        double getEtaHeight() const { return m_etaHeight; }
        
        //virtual ~DetectorGeometry();

        double m_position[3];
        double m_theta;
        double m_area, m_originY, m_aperture;

        int m_numberPartitions;
        double m_etaHeight;

        /* Get coordinates for eta partitions */
        double getY(int eta) const;
        double getYTop(int eta) const;
        double getWidth(int eta) const;
        double getStripPitch(int eta) const;
        double getStripPitchSqrt12(int eta) const;
       
        /* Getter functions */
        double getChamber() const { return m_chamber; }
        double getBaseNarrow() const { return m_baseNarrow; }
        double getBaseWide() const { return m_baseWide; }
        double getHeight() const { return m_height; }
        double getNReadoutElements() const { return m_numberReadoutElements; }
        std::string getReadoutType() const { return m_readoutType; }
   
        /* Width for a set y position */
        double getWidth(double y) const;
    private:

        int m_chamber;
        double m_baseNarrow, m_baseWide, m_height;
        int m_numberReadoutElements;
        std::string m_readoutType;

        std::vector<double> m_partitionYs;
        std::vector<double> m_partitionYTops;
        std::vector<double> m_partitionWidths;
        std::vector<double> m_partitionStripPitches;
        std::vector<double> m_partitionStripPitchesSqrt12;
};

#endif
