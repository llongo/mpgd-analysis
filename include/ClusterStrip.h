#include <vector>

#include "Cluster.h"
#include "DigiStrip.h"

#ifndef DEF_CLUSTER_STRIP
#define DEF_CLUSTER_STRIP

class ClusterStrip : public Cluster {

    public:

        /**
         * Constructor for cluster of size 1;
         * 
         * @param digi is the only digi in the cluster
         */
        ClusterStrip(int chamber, DigiStrip digi) :
            Cluster(chamber),
            m_eta{digi.getEta()}, m_first{digi}, m_last{digi},
            m_charge{digi.getCharge()},
            m_chargeTimesPosition{digi.getCharge()*digi.getStrip()},
            m_timeSum{digi.getTime()} {}

        bool isNeighbour(DigiStrip d);
        void extend(DigiStrip d);

        double getCenter();
        double getSize();
        double getCharge();
        double getTime();

        int getEta() { return m_eta; }
        DigiStrip getFirst() { return m_first; }
        DigiStrip getLast() { return m_last; }

        std::ostream& operator<<(std::ostream &os) {
            return os << "chamber " << m_chamber << " eta " << m_eta << " first " << m_first.getStrip() << " last " << m_last.getStrip();
        }

        /**
         * Return set of clusters from all the digis in the event
         *
         * @param vector of digis in event
         */
        static std::vector<ClusterStrip> fromDigis(std::vector<DigiStrip> digis);
        
    private:

        int m_eta;
        /**
         * Strip charge,
         * equal to sum of charges of all components
         */
        int m_charge;
        /**
         * Sum of charge*position for each components,
         * to be used to calculated weighted center
         */
        int m_chargeTimesPosition;
        /**
         * Sum of time of all components,
         * to use to calculate cluster time as average
         */
        int m_timeSum = 0;
        DigiStrip m_first, m_last;
};

#endif
