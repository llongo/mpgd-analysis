#include <iostream>

#ifndef DEF_DIGI
#define DEF_DIGI

class Digi {

    public:

        Digi(int chamber, int charge, int time) : m_chamber{chamber}, m_charge{charge}, m_time{time} {}
        Digi(int chamber) : Digi(chamber, 1, 1) {}

        int getChamber() { return m_chamber; }
        int getCharge() { return m_charge; }
        int getTime() { return m_time; }

        std::ostream& operator<<(std::ostream &os) {
            return os << "chamber " << m_chamber << " charge " << m_charge << " time " << m_time;
        }

    protected:
        int m_chamber, m_charge, m_time;
};

#endif
