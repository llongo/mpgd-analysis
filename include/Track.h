#include <vector>

#include "Rechit.h"
#include "Hit.h"
#include "DetectorGeometry.h"
#include "DetectorLayer.h"
#include "SetupGeometry.h"

#ifndef DEF_TRACK
#define DEF_TRACK

class Track {

    public:

        Track();

        void addRechit(Rechit);

        /* @brief Empty the rechit list */
        void clear();
        
        /* @brief Fit a linear track using the information on all rechits
         *
         * @param coordinate in which to fit (0 for x, 1 for y)
         * @returns whether the covariance is positive. TODO: return better flag for failed fits
         */
        bool fit(int direction);

        /* @brief Fit in both directions
         *
         * @returns whether both fits were successful 
         */
        bool fit();
        
        /* @brief Return the x or y position of the track extrapolated to the z plane 
         *
         * @param z is the position of the extrapolation plane 
         * @param direction is 0 for x and 1 for y 
         * @returns position in the extrapolation plane in x or y
         */
        double propagate(double z, int direction);

        /* @brief Return the x or y position of the track extrapolated to the z plane
         *
         * @param setup is the geometry to use to determine the output detector
         * @param layer is the DetectorLayer determining the plane of extrapolation 
         * @return extrapolated hit in global coordinate on the detector plane
         */
        Hit propagate(SetupGeometry *setup, DetectorLayer *layer);

        /* @brief Return the uncertainty in the x position of the track extrapolated to the z plane 
         *
         * To be fixed, last I checked this is not making much sense
         */
        double propagationError(double z, int direction);
        
        /* @brief Get the x or y position of all the rechits in the tracks 
         *
         * @param direction 0 for x, 1 for y
         */
        std::vector<double> getRechitPositions(int direction);
        /* @brief Get the z position of all the rechits in the tracks */
        std::vector<double> getRechitZ();
        /* @brief Get the uncertainties on the x direction of all the rechits in the tracks
         *
         * @param 0 for x, 1 for y
         */
        std::vector<double> getRechitErrors(int direction);

        double getInterceptX() {return m_intercept[0];}
        double getInterceptY() {return m_intercept[1];}
        double getSlopeX() {return m_slope[0];}
        double getSlopeY() {return m_slope[1];}
        double getInterceptXError() {return m_interceptError[0];}
        double getInterceptYError() {return m_interceptError[1];}
        double getSlopeXError() {return m_slopeError[0];}
        double getSlopeYError() {return m_slopeError[1];}
        double getCovarianceX() {return m_covariance[0];}
        double getCovarianceY() {return m_covariance[1];}
        double getChi2X() {return m_chi2[0];}
        double getChi2Y() {return m_chi2[1];}
        double getChi2() {return m_chi2[0] + m_chi2[1];}
        bool isValid() {return m_isValid[0] && m_isValid[1];}
        double getChi2Reduced();

    private:
    
        std::vector<Rechit> m_rechits;
        double m_intercept[2], m_slope[2];
        double m_interceptError[2], m_slopeError[2];
        double m_chi2[2], m_covariance[2];
        bool m_isValid[2];
};

#endif
