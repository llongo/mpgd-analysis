#ifndef DEF_HIT
#define DEF_HIT

#include "DetectorGeometry.h"

class Hit {

    public:
        Hit() {}
        Hit(double x, double y, double z, double errX, double errY, double errZ);
        Hit(DetectorGeometry *detector, double x, double y, double z, double errX, double errY, double errZ);
        
        static Hit fromLocal(DetectorGeometry *detector, double x, double y, double errX, double errY, double errZ);

        void setDetector(DetectorGeometry *detector);

        double getGlobalX() {return m_globalPosition[0];}
        double getGlobalY() {return m_globalPosition[1];}
        double getGlobalZ() {return m_globalPosition[2];}
        double getErrorX() {return m_errPosition[0];}
        double getErrorY() {return m_errPosition[1];}
        double getErrorZ() {return m_errPosition[2];}
        double getLocalX() {return m_localPosition[0];}
        double getLocalY() {return m_localPosition[1];}
        double getLocalR() {return m_localR;}
        double getLocalPhi() {return m_localPhi;}

        /**
         * Determines if the hit is contained in its chamber surface
         */
        bool isContained();

        int getChamber();
        int getEta();

    private:
        double m_globalPosition[3];
        double m_localPosition[2];
        double m_localR, m_localPhi;
        double m_errPosition[3];
        DetectorGeometry *m_detector;
};

#endif
