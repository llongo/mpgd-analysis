#include <vector>

#include "Cluster.h"
#include "DigiPad.h"

#ifndef DEF_CLUSTER_PADS
#define DEF_CLUSTER_PADS

class ClusterPad : public Cluster {

    public:

        /**
         * Constructor for cluster of size 1;
         * 
         * @param digi is the only digi in the cluster
         */
        ClusterPad(int chamber, DigiPad digi) : Cluster(chamber), m_digis{std::vector<DigiPad>{digi}} {}

        /**
         * Constructor from list of pads
         */
        ClusterPad(int chamber, std::vector<DigiPad> digis) : Cluster(chamber), m_digis{digis} {}

        bool isNeighbour(DigiPad d);
        void extend(DigiPad d);

        double getCenterX();
        double getCenterY();
        double getSize();
        int getCharge();
        int getTime();

        std::vector<DigiPad> getPads() { return m_digis; }
    
        std::ostream& operator<<(std::ostream &os) {
            os << "chamber " << m_chamber << " pads ";
            for (DigiPad digi: m_digis) {
                os << "(" << digi.getPadX() << "," << digi.getPadY() << ") ";
            }
            return os;
        }
        
        /**
         * Return set of clusters from all the digis in the event
         *
         * @param vector of digis in event
         */
        static std::vector<ClusterPad> fromDigis(std::vector<DigiPad> digis);

    private:

        std::vector<DigiPad> m_digis;
};

#endif
