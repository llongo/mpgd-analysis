#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <stdexcept>

#include "SetupGeometry.h"
#include "DataFrame.h"
#include "tinyxml2.h"

#ifdef DEF_SETUP_GEOMETRY
#define DEF_SETUP_GEOMETRY

SetupGeometry::SetupGeometry(std::string geometryFile) {

    tinyxml2::XMLDocument setupGeometryDoc;
    if (setupGeometryDoc.LoadFile(geometryFile.c_str())) {
        throw std::invalid_argument("Could not open setup geometry file " + geometryFile + ". Does the file exist?");
    }

    tinyxml2::XMLElement *setupElement = setupGeometryDoc.FirstChildElement("setup");

    const char *setupLabel;
    setupElement->QueryStringAttribute("label", &setupLabel);
    std::cout << setupLabel << std::endl;

    tinyxml2::XMLElement *detectorElement = setupElement->FirstChildElement("detector");
    while (detectorElement) {
        const char *chamberLabel;
        int detectorID;
        bool isTracker = false;
        const char *readoutType; // strips or pads
        double baseSmall, baseLarge, height;
        int nEta, nReadoutElements;
        double x, y, z, phi_z;

        detectorElement->QueryStringAttribute("label", &chamberLabel);
        detectorElement->QueryBoolAttribute("tracker", &isTracker);
        detectorElement->QueryIntAttribute("id", &detectorID);

        const char *geometryName = detectorElement->FirstChildElement("geometry")->GetText();
        // Read detector geometry from corresponding file:
        std::string detectorGeometryPath = "geometry/detectors/"+std::string(geometryName)+".xml";
        tinyxml2::XMLDocument detectorGeometryDoc;
        if (detectorGeometryDoc.LoadFile(detectorGeometryPath.c_str())) {
            throw std::invalid_argument("Could not open detector geometry file " + detectorGeometryPath + ". Does the file exist?");
        }
        tinyxml2::XMLElement *geometryElement = detectorGeometryDoc.FirstChildElement("detectorGeometry")->FirstChildElement("geometry");
        geometryElement->FirstChildElement("baseSmall")->QueryDoubleText(&baseSmall);
        geometryElement->FirstChildElement("baseLarge")->QueryDoubleText(&baseLarge);
        geometryElement->FirstChildElement("height")->QueryDoubleText(&height);

        tinyxml2::XMLElement *readoutElement = detectorGeometryDoc.FirstChildElement("detectorGeometry")->FirstChildElement("readout");
        readoutElement->QueryStringAttribute("type", &readoutType);
        readoutElement->FirstChildElement("nEta")->QueryIntText(&nEta);
        if (std::string(readoutType)=="strips") {
            readoutElement->FirstChildElement("nStrips")->QueryIntText(&nReadoutElements);
        } else if (std::string(readoutType)=="pads") {
            readoutElement->FirstChildElement("nPads")->QueryIntText(&nReadoutElements);
        }

        // Read alignment from detector geometry file:
        tinyxml2::XMLElement *alignmentElement = detectorElement->FirstChildElement("alignment");
        alignmentElement->FirstChildElement("x")->QueryDoubleText(&x);
        alignmentElement->FirstChildElement("y")->QueryDoubleText(&y);
        alignmentElement->FirstChildElement("z")->QueryDoubleText(&z);
        alignmentElement->FirstChildElement("phi_z")->QueryDoubleText(&phi_z);

        DetectorGeometry detector(
            detectorID, baseSmall, baseLarge, height, nEta, readoutType, nReadoutElements
        );
        detector.setPosition(
            x, y, z, phi_z
        );
        detectorMap[detector.getChamber()] = detector;
        if (isTracker) trackerChambers.push_back(detector.getChamber());
        detectorMap[detector.getChamber()].print();

        // Go on to next detector
        detectorElement = detectorElement->NextSiblingElement("detector");
    }

    /* Group together detectors by equal z */
    for (auto detectorPair:detectorMap) {
        double z = detectorPair.second.getPositionZ();
        if (!layerMap.count(z)) {
            layerMap[z] = std::vector<int>();
        }
        layerMap.at(z).push_back(detectorPair.first);
    }
    std::cout << "-------------------------------------------" << std::endl;
    std::cout << "List of detectors at same z position:" << std::endl;
    int layerIndex = 0;
    for (auto layerPair:layerMap) {
        layers.push_back(DetectorLayer(layerPair.first));
        std::cout << "    Detectors at z position " << layerPair.first << ": ";
        for (auto d:layerPair.second) {
            layers.back().addChamber(d);
            layers.back().setTracking(layers.back().isTracking()||isTrackingChamber(d));
            detectorToLayerMap[d] = layerIndex;
            std::cout << d << " ";
        }
        std::cout << "is tracking: " << layers.back().isTracking();
        std::cout << std::endl;
        layerIndex++;
    }
    std::cout << "Map of detector layers: ";
    for (auto detectorLayerPair:detectorToLayerMap) {
        std::cout << "{" << detectorLayerPair.first << ":" << getLayer(detectorLayerPair.first) << "} ";
    }
    std::cout << std::endl;
    std::cout << "-------------------------------------------" << std::endl;
}

bool SetupGeometry::isTrackingChamber(int chamber) {
    return std::count(trackerChambers.begin(), trackerChambers.end(), chamber)>0;
}

bool SetupGeometry::isTrackingLayer(int layer) {
    return layers[layer].isTracking();
}

DetectorGeometry *SetupGeometry::getChamber(int chamber) {
    if (detectorMap.count(chamber)>0) {
        return &detectorMap.at(chamber);
    } else {
        throw std::invalid_argument(std::string("The detector number " + std::to_string(chamber) + " does not exist or has no mapping."));
    }
}

int SetupGeometry::getLayer(int chamber) {
    return detectorToLayerMap[chamber];
}

void SetupGeometry::print() {
    geometryDataFrame.print();
}

#endif
