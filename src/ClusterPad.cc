#include <iostream>
#include <vector>

#include "ClusterPad.h"
#include "DigiPad.h"

bool ClusterPad::isNeighbour(DigiPad otherDigi) {
    if ( (m_chamber != otherDigi.getChamber()) ) return false;
    for (DigiPad digi: m_digis) {
        if (digi.isNeighbour(otherDigi)) return true;
    }
    return false;
}

void ClusterPad::extend(DigiPad digi) {
    m_digis.push_back(digi);
}

double ClusterPad::getCenterX() {
    /* Return geometrical center */
    double center = 0, charge = 0;
    for (auto digi: m_digis) {
        center += ((double) digi.getPadX()) * ((double) digi.getCharge());
        charge += (double) digi.getCharge();
    }
    return center / charge;
}

double ClusterPad::getCenterY() {
    /* Return geometrical center */
    double center = 0, charge = 0;
    for (auto digi: m_digis) {
        center += ((double) digi.getPadY()) * ((double) digi.getCharge());
        charge += (double) digi.getCharge();
    }
    return center / charge;
}

int ClusterPad::getCharge() {
    int charge = 0;
    for (auto digi: m_digis) {
        charge += digi.getCharge();
    }
    return charge;
}

int ClusterPad::getTime() {
    int time = 0;
    for (auto digi: m_digis) {
        time += digi.getTime();
    }
    return time;
}

double ClusterPad::getSize() {
    return sqrt(m_digis.size());
}

std::vector<ClusterPad> ClusterPad::fromDigis(std::vector<DigiPad> digis) {
    std::vector<ClusterPad> clusters;

    int chamber;

    while (digis.size()>0) {
        DigiPad digi = digis[0];
        chamber = digi.getChamber();

        // Use first digi as seed for "proto-cluster" of size 1:
        ClusterPad cluster = ClusterPad(chamber, digi);
        digis.erase(digis.begin());

        bool clusterUpdated = true;
        while (clusterUpdated) {
            clusterUpdated = false;
            for (int j=0; j<digis.size();) {
                if (digis[j].getChamber() != cluster.getChamber()) break;
                if (cluster.isNeighbour(digis[j])) {
                    cluster.extend(digis[j]);
                    digis.erase(digis.begin() + j); // Don't increase j, list fill flow
                    /** 
                     * The cluster ends grew, so there might be more strips included;
                     * then scan the digi list again later:
                     */
                    clusterUpdated = true;
                } else j++;
            }
        }
        clusters.push_back(cluster);
    }
    return clusters;
}
