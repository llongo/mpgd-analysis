#include <iostream>
#include <vector>

#include "ClusterStrip.h"
#include "DigiStrip.h"

bool ClusterStrip::isNeighbour(DigiStrip digi) {
    if ( (m_chamber != digi.getChamber()) || (m_eta != digi.getEta()) ) return false;
    return (digi.getStrip()==m_first.getStrip()-1) || (digi.getStrip()==m_last.getStrip()+1);
}

void ClusterStrip::extend(DigiStrip digi) {
    if (digi.getStrip() < m_first.getStrip()) m_first = digi;
    else if (digi.getStrip() > m_last.getStrip()) m_last = digi;
    m_charge += digi.getCharge();
    m_chargeTimesPosition += digi.getCharge() * digi.getStrip();
}

double ClusterStrip::getCenter() {
    //return 0.5*(m_first.getStrip() + m_last.getStrip());
    return m_chargeTimesPosition / m_charge;
}

double ClusterStrip::getCharge() {
    return m_charge;
}

double ClusterStrip::getTime() {
    return m_timeSum / getSize();
}

double ClusterStrip::getSize() {
    return m_last.getStrip() - m_first.getStrip() + 1;
}

std::vector<ClusterStrip> ClusterStrip::fromDigis(std::vector<DigiStrip> digis) {
    std::vector<ClusterStrip> clusters;

    int chamber;

    while (digis.size()>0) {
        DigiStrip digi = digis[0];
        chamber = digi.getChamber();

        // Use first digi as seed for "proto-cluster" of size 1:
        ClusterStrip cluster = ClusterStrip(chamber, digi);
        digis.erase(digis.begin());

        bool clusterUpdated = true;
        while (clusterUpdated) {
            clusterUpdated = false;
            for (int j=0; j<digis.size();) {
                if (digis[j].getChamber() != cluster.getChamber()) break;
                if (cluster.isNeighbour(digis[j])) {
                    cluster.extend(digis[j]);
                    digis.erase(digis.begin() + j); // Don't increase j, list fill flow
                    /** 
                     * The cluster ends grew, so there might be more strips included;
                     * then scan the digi list again later:
                     */
                    clusterUpdated = true;
                } else j++;
            }
        }
        clusters.push_back(cluster);
    }
    return clusters;
}
