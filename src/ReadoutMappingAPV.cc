#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>

#include "ReadoutMappingAPV.h"
#include "DataFrame.h"

ReadoutMappingAPV::ReadoutMappingAPV(std::string mappingFilePath) : m_mappingFilePath{mappingFilePath} {
	DataFrame mappingDataFrame = DataFrame::fromCsv(mappingFilePath);
	int fecId, chipId, channelId; 

	// iterate on rows:
	for (int irow=0; irow<mappingDataFrame.getNRows(); irow++) {
		fecId = std::stoi(mappingDataFrame.getElement("fec", irow));
		chipId = std::stoi(mappingDataFrame.getElement("chip", irow));
		channelId = std::stoi(mappingDataFrame.getElement("channel", irow));

        FedChipChannelTuple p{fecId, chipId, channelId};
		toChamber[p] = std::stoi(mappingDataFrame.getElement("chamber", irow));
		toType[p] = std::stoi(mappingDataFrame.getElement("type", irow)); // 0 for strip, 1 for pad
		toPadX[p] = std::stoi(mappingDataFrame.getElement("pad_x", irow));
		toPadY[p] = std::stoi(mappingDataFrame.getElement("pad_y", irow));
		toStrip[p] = std::stoi(mappingDataFrame.getElement("strip", irow));
	}
}

void ReadoutMappingAPV::print() {
    std::cout << "fec\tchip\tchannel\ttype\tpad x\tpad y\tstrip" << std::endl;
    FedChipChannelTuple p;
    int lineIndex = 0;
    int lastLine = toType.size();
    for (auto mapIterator=toType.begin(); mapIterator!=toType.end(); mapIterator++) {
        p = mapIterator->first;
        if ((lineIndex<10)||(lastLine-lineIndex<10)) {
            std::cout << std::get<0>(p) << "\t" << std::get<1>(p) << "\t" << std::get<2>(p) << "\t" << toType[p] << "\t" << toPadX[p] << "\t" << toPadY[p] << "\t" << toStrip[p] << std::endl;
        } else if (lineIndex==10) {
            std::cout << "..." << std::endl;
        }
        lineIndex++;
    }
    std::cout << std::endl;
}
