#include <iostream>
#include <vector>
#include <stdexcept>
#include <math.h>

#include "Minuit2/MnUserParameters.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/FunctionMinimum.h"

#include "Rechit.h"
#include "Track.h"
#include "LinearFcn.h"

#define USE_LEAST_SQUARES

Track::Track() {}

void Track::addRechit(Rechit rechit) {
    m_rechits.push_back(rechit);
}

void Track::clear() {
    m_rechits.clear();
}

std::vector<double> Track::getRechitPositions(int direction) {
    std::vector<double> positions;
    for (auto rechit:m_rechits) {
        if (direction == 0) {
            positions.push_back(rechit.getGlobalX());
        } else if (direction == 1) {
            positions.push_back(rechit.getGlobalY());
        } else {
            throw std::invalid_argument(std::string("Direction " + std::to_string(direction) + " not recognized"));
        }
    }
    return positions;
}

std::vector<double> Track::getRechitZ() {
    std::vector<double> zs;
    for (auto rechit:m_rechits) {
        zs.push_back(rechit.getGlobalZ());
    }
    return zs;
}

std::vector<double> Track::getRechitErrors(int direction) {
    std::vector<double> errors;
    for (auto rechit:m_rechits) {
        if (direction == 0) {
            errors.push_back(rechit.getErrorGlobalX());
        } else if (direction == 1) {
            errors.push_back(rechit.getErrorGlobalY());
        } else {
            throw std::invalid_argument(std::string("Direction " + std::to_string(direction) + " not recognized"));
        }
    }
    return errors;
}

bool Track::fit(int direction) {
    auto positions = getRechitPositions(direction);
    auto errors = getRechitErrors(direction);
    auto z = getRechitZ();
    LinearFcn chi2Fcn(positions, z, errors);

#if defined(USE_MINUIT)
    ROOT::Minuit2::MnUserParameters initialPars;
    initialPars.Add("intercept", 0., 0.1);
    initialPars.Add("slope", 0., 0.1);
    ROOT::Minuit2::MnMigrad migrad(chi2Fcn, initialPars);
    ROOT::Minuit2::FunctionMinimum min = migrad();

    m_chi2[direction] = min.Fval();
    m_isValid[direction] = migrad.State().CovarianceStatus()>0;

    m_intercept[direction] = migrad.Value("intercept");
    m_slope[direction] = migrad.Value("slope");
    m_interceptError[direction] = migrad.Error("intercept");
    m_slopeError[direction] = migrad.Error("slope");
    if (m_isValid[direction]) m_covariance[direction] = migrad.Covariance().Data()[1];
#elif defined(USE_LEAST_SQUARES)
    float t1{0.}, t2{0.}, t3{0.}, t4{0.}, t5{0.};
    for (int i=0; i<positions.size(); i++) {
        //if (errors.at(i)>10) continue; // Unused: do not use rechit y information
        t1 += 1/pow(errors.at(i),2);
        t2 += pow(z.at(i)/errors.at(i),2);
        t3 += z.at(i)/pow(errors.at(i),2);
        t4 += positions.at(i)/pow(errors.at(i),2);
        t5 += positions.at(i)*z.at(i)/pow(errors.at(i),2);
    }
    float delta = t1*t2-pow(t3,2);

    m_intercept[direction] = (t2*t4-t3*t5)/delta;
    m_slope[direction] = (t1*t5-t3*t4)/delta;
    m_interceptError[direction] = sqrt(t2/delta);
    m_slopeError[direction] = sqrt(t1/delta);
    m_covariance[direction] = m_interceptError[direction]*m_slopeError[direction];

    m_isValid[direction] = 1;
    m_chi2[direction] = 0;
    for (int i=0; i<positions.size(); i++){
        m_chi2[direction] += pow(positions.at(i) - z.at(i)*m_slope[direction] - m_intercept[direction], 2);
    }
#endif

    /*std::cout << "    i\txy\tposition\terror\tz" << std::endl;
    for (int i=0; i<positions.size(); i++) {
        std::cout << "    " << i << "\t";
        if (direction==0) std::cout << "x"; else if (direction==1) std::cout << "y"; std::cout << "\t";
        std::cout << positions[i] << "\t";
        std::cout << errors[i] << "\t";
        std::cout << z[i];
        std::cout << std::endl;
    }

    std::cout << "    intercept " << pow(migrad.Error("intercept"), 2) << std::endl;
    std::cout << "    slope " << pow(migrad.Error("slope"), 2) << std::endl;    
    std::cout << "    chi2 " << m_chi2[direction] << std::endl;    
    std::cout << "    valid " << m_isValid[direction] << std::endl;    
    std::cout << std::endl;*/
    // auto cov = covarianceData[1];
    // auto sigma1 = covarianceData[0];
    // auto sigma2 = covarianceData[2];
    return m_isValid[direction];
}

bool Track::fit() {
    return fit(0) && fit(1);
}

double Track::propagate(double z, int direction) {
    return m_intercept[direction] + z*m_slope[direction];
}

Hit Track::propagate(SetupGeometry *setup, DetectorLayer *layer) {
    Hit prophit(
            propagate(layer->getPositionZ(), 0),
            propagate(layer->getPositionZ(), 1),
            layer->getPositionZ(),
            0., 0., 0. // TODO: use proper extrapolation errors
            );
    /* Determine the chamber the hit belongs to */
    for (int chamber:layer->getChambers()) {
        prophit.setDetector(setup->getChamber(chamber));
        if (prophit.isContained()) {
            return prophit;
        }
    }
    // if no chamber contains the hit, throw error
    throw std::domain_error("No chamber contains the propagated hit");
}

double Track::propagationError(double z, int direction) {
    // std::cout << "    z " << z << " ";
    // std::cout << "independent " << sqrt(pow(m_interceptError, 2) + pow(z*m_slopeError, 2)) << " ";
    // std::cout << "correlated " << sqrt(pow(m_interceptError, 2) + pow(z*m_slopeError, 2) + 2*z*m_covariance) << std::endl;
    return sqrt(pow(m_interceptError[direction], 2) + pow(z*m_slopeError[direction], 2) + 2*z*m_covariance[direction]);
}

double Track::getChi2Reduced() {
    return (m_chi2[0]+m_chi2[1])/(m_rechits.size()-2);
}
