#include <math.h>
#include <stdexcept>

#include "DetectorGeometry.h"

DetectorGeometry::DetectorGeometry(const DetectorGeometry& detector) {
    DetectorGeometry(
            detector.getChamber(),
            detector.getBaseNarrow(), detector.getBaseWide(), detector.getHeight(),
            detector.getNEta(),
            detector.getReadoutType(),
            detector.getNReadoutElements()
            );
    setPosition(detector.getPositionX(), detector.getPositionY(), detector.getPositionZ(), detector.getTheta());
}

DetectorGeometry::DetectorGeometry(int chamber, double baseNarrow, double baseWide, double height, int nEta, std::string readoutType, int nReadoutElements) {
    m_chamber = chamber;
    m_baseNarrow = baseNarrow;
    m_baseWide = baseWide;
    m_height = height;
    m_numberPartitions = nEta;
    m_readoutType = readoutType;
    m_numberReadoutElements = nReadoutElements;
    m_etaHeight = height/nEta;

    if (m_readoutType != "strips" && m_readoutType != "pads") {
        throw std::invalid_argument("Could not recognize readout type " + std::string(m_readoutType) + " for detector " + std::to_string(chamber));
    }

    m_partitionYs.reserve(m_numberPartitions);
    m_partitionYTops.reserve(m_numberPartitions);
    m_partitionWidths.reserve(m_numberPartitions);
    m_partitionStripPitches.reserve(m_numberPartitions);
    m_partitionStripPitchesSqrt12.reserve(m_numberPartitions);
    for (int eta=0; eta<m_numberPartitions; eta++) {
        m_partitionYs.push_back( -0.5*m_height + m_etaHeight*(0.5 + (double)(m_numberPartitions-eta-1)));
        m_partitionYTops.push_back(m_partitionYs[eta] + 0.5*m_etaHeight);
        m_partitionWidths.push_back(m_baseNarrow + (m_partitionYs[eta]+0.5*m_height)*(m_baseWide-m_baseNarrow)/m_height);
        if (m_readoutType=="strips") {
            m_partitionStripPitches.push_back(m_partitionWidths[eta] / m_numberReadoutElements);
        } else if (m_readoutType=="pads") {
            m_partitionStripPitches.push_back(m_partitionWidths[eta] / ((int) sqrt(m_numberReadoutElements)+1));
        }
        m_partitionStripPitchesSqrt12.push_back(m_partitionStripPitches[eta] * INVERSE_SQRT_12);
    }

    // Calculate detector geometric parameters:
    m_originY = - baseNarrow*height/(baseWide-baseNarrow) - 0.5*m_height;
    m_area = 0.5*(baseWide+baseNarrow)*height;
    m_aperture = 2*atan(0.5*baseWide/abs(m_originY-0.5*m_height));
}

DetectorGeometry::DetectorGeometry(int chamber, double baseNarrow, double baseWide, double height, int nEta, std::string readoutType, int nReadoutElements, double x, double y, double z, double theta)
:DetectorGeometry(chamber, baseNarrow, baseWide, height, nEta, readoutType, nReadoutElements) {
    setPosition(x, y, z, theta);
}

void DetectorGeometry::print() const {
    std::cout << "-----------------------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "Detector " << m_chamber << std::endl;
    std::cout << "Readout type: " << m_readoutType << std::endl;
    std::cout << "Narrow base " << m_baseNarrow << ", wide base " << m_baseWide << ", height " << m_height << std::endl;
    std::cout << "Eta partitions " << m_numberPartitions << ", " << m_readoutType << " " << m_numberReadoutElements << ", eta partition height " << m_etaHeight << std::endl;

    for (int eta=0; eta<m_numberPartitions; eta++) {
        std::cout << "    eta partition " << eta+1;
        std::cout << ", middle y " << m_partitionYs[eta] << ", width " << m_partitionWidths[eta];
        std::cout << ", strip pitch " << m_partitionStripPitches[eta];
        std::cout << ", expected resolution " << m_partitionStripPitchesSqrt12[eta] << std::endl;
    }

    // Print detector geometric parameters:
    std::cout << "Origin " << m_originY;
    std::cout << ", area " << m_area;
    std::cout << ", aperture " << m_aperture;
    std::cout << ", pitch " << m_aperture/m_numberReadoutElements;
    std::cout << ", expected resolution " << m_aperture/m_numberReadoutElements/pow(12, 0.5) << std::endl;

    std::cout << "Position (";
    std::cout << m_position[0] << ", " << m_position[1] << ", " << m_position[2];
    std::cout << "), angle ";
    std::cout << m_theta << std::endl;
}

double DetectorGeometry::getY(int eta) const {
    return m_partitionYs[eta-1];
}

double DetectorGeometry::getYTop(int eta) const {
    return m_partitionYTops[eta-1];
}

double DetectorGeometry::getWidth(int eta) const {
    return m_partitionWidths[eta-1];
}

double DetectorGeometry::getStripPitch(int eta) const {
    return m_partitionStripPitches[eta-1];
}

double DetectorGeometry::getStripPitchSqrt12(int eta) const {
    return m_partitionStripPitchesSqrt12[eta-1];
}

Rechit DetectorGeometry::createRechit(ClusterStrip cluster) const {
    double x, y, errorX, errorY;
    x = -0.5*getWidth(cluster.getEta()) + getStripPitch(cluster.getEta()) * cluster.getCenter();
    y = getY(cluster.getEta());
    errorX = cluster.getSize() * getStripPitchSqrt12(cluster.getEta()); // uncertainty on x from cluster size
    errorY = m_etaHeight * 0.5; // uncertainty on y from eta partition height. TODO: support different eta heights
    Rechit rechit(m_chamber, x, y, errorX, errorY, cluster.getSize(), cluster.getCharge(), cluster.getTime());
    rechit.setR(sqrt(pow(x,2) + pow(y-getOriginY(),2)));
    rechit.setPhi(atan(x/(y-getOriginY())));
    return rechit;
}

Rechit DetectorGeometry::createRechit(ClusterPad cluster) const {
    double x, y, errorX, errorY;
    x = -0.5*getWidth(1) + getStripPitch(1) * cluster.getCenterX();
    y = -0.5*getHeight() + getStripPitch(1) * cluster.getCenterY();
    // uncertainty on both x and y from cluster size
    errorX = cluster.getSize() * getStripPitchSqrt12(1); 
    errorY = cluster.getSize() * getStripPitchSqrt12(1);
    Rechit rechit(m_chamber, x, y, errorX, errorY, cluster.getSize(), cluster.getCharge(), cluster.getTime());
    rechit.setR(sqrt(pow(x,2) + pow(y-getOriginY(),2)));
    rechit.setPhi(atan(x/(y-getOriginY())));
    return rechit;
}

void DetectorGeometry::mapRechit(Rechit *rechit) {
    double localX = rechit->getX();
    double localY = rechit->getY();
    double c = cos(m_theta);
    double s = sin(m_theta);
    double errorX = rechit->getErrorX();
    double errorY = rechit->getErrorY();
    rechit->setR(sqrt(pow(localX,2) + pow(localY-getOriginY(),2)));
    rechit->setPhi(atan(localX/(localY-getOriginY())));
    rechit->setGlobalPosition(
            m_position[0] + localX*c - localY*s,
            m_position[1] + localX*s + localY*c,
            m_position[2],
            sqrt(pow(c*errorX,2) + pow(s*errorY,2)),
            sqrt(pow(s*errorX,2) + pow(c*errorY,2))
            );
}

double DetectorGeometry::getWidth(double y) const {
    return m_baseNarrow + y*(m_baseWide-m_baseNarrow)/m_height;
}

