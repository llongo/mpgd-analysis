#include <math.h>

#include "Cluster.h"
#include "Rechit.h"
#include "DetectorLayer.h"

DetectorLayer::DetectorLayer(double z) {
    m_z = z;
}

DetectorLayer::DetectorLayer(double z, std::vector<int> chambers) {
    m_z = z;
    m_chambers = chambers;
    /*for (auto chamber:chambers) {
        m_isTracking = m_isTracking || (setupGeometry->isTracking(chamber));
    }*/
}

void DetectorLayer::addChamber(int chamber) {
    m_chambers.push_back(chamber);
    //m_isTracking = m_isTracking || (setupGeometry->isTracking(chamber));
}

std::vector<int> DetectorLayer::getChambers() {
    return m_chambers;
}

void DetectorLayer::setTracking(bool tracking) {
    m_isTracking = tracking;
}

bool DetectorLayer::isTracking() {
    return m_isTracking;
}

double DetectorLayer::getPositionZ() {
    return m_z;
}
