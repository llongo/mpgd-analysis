#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>

#include "mapping/vfat/ChamberMapping.h"
#include "DataFrame.h"

ChamberMapping::ChamberMapping(std::string mappingFilePath) : Mapping{mappingFilePath} {
    DataFrame mappingDataFrame = DataFrame::fromCsv(getMappingFilePath());
    int fed, slot, oh, vfat;

    /**
     * Read mapping files, then fill mapping maps
     */
    for (int irow=0; irow<mappingDataFrame.getNRows(); irow++) {
        fed = std::stoi(mappingDataFrame.getElement("fed", irow));
        slot = std::stoi(mappingDataFrame.getElement("slot", irow));
        oh = std::stoi(mappingDataFrame.getElement("oh", irow));
        vfat = std::stoi(mappingDataFrame.getElement("vfat", irow));

        ChamberMappingKey p{fed, slot, oh, vfat};
        toChamber[p] = std::stoi(mappingDataFrame.getElement("chamber", irow));
    }
}

void ChamberMapping::print() {
    std::cout << "fed\tslot\toh\tvfat\tchamber" << std::endl;
    ChamberMappingKey p;
    int lineIndex = 0;
    int lastLine = toChamber.size();
    for (auto mapIterator=toChamber.begin(); mapIterator!=toChamber.end(); mapIterator++) {
        p = mapIterator->first;
        if ((lineIndex<10)||(lastLine-lineIndex<10)) {
            std::cout << std::get<0>(p) << "\t" << std::get<1>(p) << "\t" << std::get<2>(p) << "\t" << std::get<3>(p) << "\t" << toChamber[p] << std::endl;
        } else if (lineIndex==10) {
            std::cout << "..." << std::endl;
        }
        lineIndex++;
    }
    std::cout << std::endl;
}

