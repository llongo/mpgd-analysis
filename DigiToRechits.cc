#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <array>
#include <bitset>
#include <signal.h>

#include <TFile.h>
#include <TTree.h>

#include "DigiStrip.h"
#include "DigiPad.h"
#include "ClusterStrip.h"
#include "ClusterPad.h"
#include "DetectorGeometry.h"
#include "SetupGeometry.h"

#include "progressbar.h"

bool isInterrupted = false;
void interruptHandler(int dummy) {
    isInterrupted = true;
}

int main (int argc, char** argv) {

    if (argc<3) {
        std::cout << "Usage: DigiToRechits ifile ofile [--verbose] [--events n] [--geometry geometry_name]" << std::endl;
        return 0;
    }
    std::string ifile   = argv[1];
    std::string ofile   = argv[2];

    int max_events = -1;
    bool verbose = false;
    std::string geometry;
    for (int iarg=0; iarg<argc; iarg++) {
        std::string arg = argv[iarg];
        if (arg=="--verbose") verbose = true;
        else if (arg=="--events") max_events = atoi(argv[iarg+1]); 
        else if (arg=="--geometry") geometry = argv[iarg+1]; 
    }

    if (geometry == "") {
        std::cout << "Please specify a setup geometry" << std::endl;
        return -1;
    }

    if (max_events>=0) std::cout << "Analyzing " << max_events << " events" << std::endl;
    else std::cout << "Analyzing all events" << std::endl; 

    TFile digiFile(ifile.c_str(), "READ");
    TFile rechitFile(ofile.c_str(), "RECREATE", "Rechit tree file");

    TTree *digiTree = (TTree *) digiFile.Get("outputtree");
    TTree rechitTree("rechitTree","rechitTree");

    /* Define detector geometries */
    SetupGeometry setupGeometry("geometry/setups/"+geometry+".xml");

    // digi variables
    int orbitNumber, bunchCounter, eventCounter;
    std::vector<int> *vecRawChannel = new std::vector<int>();
    std::vector<int> *vecDigiStripChamber = new std::vector<int>();
    std::vector<int> *vecDigiStripCharge = new std::vector<int>();
    std::vector<int> *vecDigiStripTime = new std::vector<int>();
    std::vector<int> *vecDigiStripEta = new std::vector<int>();
    std::vector<int> *vecDigiStrip = new std::vector<int>();
    std::vector<int> *vecDigiPadChamber = new std::vector<int>();
    std::vector<int> *vecDigiPadCharge = new std::vector<int>();
    std::vector<int> *vecDigiPadTime = new std::vector<int>();
    std::vector<int> *vecDigiPadX = new std::vector<int>();
    std::vector<int> *vecDigiPadY = new std::vector<int>();

    /** 
     * Cluster variables:
     * a cluster of strips is defined by chamber, eta, center, size
     * a cluster of pads is defined by chamber, center x, center y, size
     */
    std::vector<int> vecClusterStripChamber;
    std::vector<int> vecClusterStripEta;
    std::vector<int> vecClusterStripCenter;
    std::vector<int> vecClusterStripSize;
    std::vector<int> vecClusterPadChamber;
    std::vector<int> vecClusterPadCenterX;
    std::vector<int> vecClusterPadCenterY;
    std::vector<int> vecClusterPadSize;

    /**
     * Rechit variables:
     * a rechit is defined by chamber, eta, x, y, error x, error y, cluster size
     * for a rechit built by pads, eta is always 1
     */
    std::vector<int> vecRechitChamber;
    std::vector<int> vecRechitEta;
    std::vector<double> vecRechitX;
    std::vector<double> vecRechitY;
    std::vector<double> vecRechitErrorX;
    std::vector<double> vecRechitErrorY;
    std::vector<double> vecRechitR;
    std::vector<double> vecRechitPhi;
    std::vector<double> vecRechitClusterSize;
    std::vector<int> vecRechitCharge;
    std::vector<int> vecRechitTime;

    // support variables
    Rechit rechit;

    std::vector<DigiStrip> digiStripsInEvent;
    std::vector<DigiPad> digiPadsInEvent;
    std::vector<ClusterStrip> clusterStripsInEvent;
    std::vector<ClusterPad> clusterPadsInEvent;

    // digi variable branches
    digiTree->SetBranchAddress("orbitNumber", &orbitNumber);
    digiTree->SetBranchAddress("bunchCounter", &bunchCounter);
    digiTree->SetBranchAddress("eventCounter", &eventCounter);
    digiTree->SetBranchAddress("rawChannel", &vecRawChannel);
    digiTree->SetBranchAddress("digiStripChamber", &vecDigiStripChamber);
    digiTree->SetBranchAddress("digiStripCharge", &vecDigiStripCharge);
    digiTree->SetBranchAddress("digiStripTime", &vecDigiStripTime);
    digiTree->SetBranchAddress("digiStripEta", &vecDigiStripEta);
    digiTree->SetBranchAddress("digiStrip", &vecDigiStrip);
    digiTree->SetBranchAddress("digiPadChamber", &vecDigiPadChamber);
    digiTree->SetBranchAddress("digiPadCharge", &vecDigiPadCharge);
    digiTree->SetBranchAddress("digiPadTime", &vecDigiPadTime);
    digiTree->SetBranchAddress("digiPadX", &vecDigiPadX);
    digiTree->SetBranchAddress("digiPadY", &vecDigiPadY);

    // event branches
    rechitTree.Branch("orbitNumber", &orbitNumber);
    rechitTree.Branch("eventCounter", &eventCounter);
    rechitTree.Branch("bunchCounter", &bunchCounter);

    // cluster branches
    rechitTree.Branch("clusterStripChamber", &vecClusterStripChamber);
    rechitTree.Branch("clusterStripEta", &vecClusterStripEta);
    rechitTree.Branch("clusterStripCenter", &vecClusterStripCenter);
    rechitTree.Branch("clusterStripSize", &vecClusterStripSize);
    rechitTree.Branch("clusterPadChamber", &vecClusterPadChamber);
    rechitTree.Branch("clusterPadCenterX", &vecClusterPadCenterX);
    rechitTree.Branch("clusterPadCenterY", &vecClusterPadCenterY);
    rechitTree.Branch("clusterPadSize", &vecClusterPadSize);

    // rechit branches
    rechitTree.Branch("rawChannel", vecRawChannel);
    rechitTree.Branch("rechitChamber", &vecRechitChamber);
    rechitTree.Branch("rechitEta", &vecRechitEta);
    rechitTree.Branch("rechitX", &vecRechitX);
    rechitTree.Branch("rechitY", &vecRechitY);
    rechitTree.Branch("rechitErrorX", &vecRechitErrorX);
    rechitTree.Branch("rechitErrorY", &vecRechitErrorY);
    rechitTree.Branch("rechitR", &vecRechitR);
    rechitTree.Branch("rechitPhi", &vecRechitPhi);
    rechitTree.Branch("rechitClusterSize", &vecRechitClusterSize);
    rechitTree.Branch("rechitCharge", &vecRechitCharge);
    rechitTree.Branch("rechitTime", &vecRechitTime);

    int nentries = digiTree->GetEntries();
    std::cout << nentries << " total events" <<  std::endl;
    if (max_events>=0) {
        std::cout << "Processing " << nentries << " events" << std::endl;
        nentries = max_events;
    }
    progressbar bar(nentries);

    signal(SIGINT, interruptHandler);
    for (int nevt=0; (!isInterrupted) && digiTree->LoadTree(nevt)>=0; ++nevt) {
        if ((max_events>=0) && (nevt>=max_events)) break;

        if (verbose) std::cout << "Event " << nevt << "/" << nentries << std::endl;
        else bar.update();
        //if ( nevt%1000==0 ) std::cout << "Unpacking event " << nevt << "\t\t\t\r";

        digiTree->GetEntry(nevt);

        vecClusterStripChamber.clear();
        vecClusterStripEta.clear();
        vecClusterStripCenter.clear();
        vecClusterStripSize.clear();
        vecClusterPadCenterX.clear();
        vecClusterPadCenterY.clear();
        vecClusterPadSize.clear();

        vecRechitChamber.clear();
        vecRechitEta.clear();
        vecRechitX.clear();
        vecRechitY.clear();
        vecRechitErrorX.clear();
        vecRechitErrorY.clear();
        vecRechitR.clear();
        vecRechitPhi.clear();
        vecRechitClusterSize.clear();
        vecRechitCharge.clear();
        vecRechitTime.clear();

        /**
         * Build strip clusters
         */
        digiStripsInEvent.clear();
        for (int ihit=0; ihit<vecDigiStrip->size(); ihit++) {
            digiStripsInEvent.push_back(DigiStrip(
                        vecDigiStripChamber->at(ihit),
                        vecDigiStripEta->at(ihit),
                        vecDigiStrip->at(ihit),
                        vecDigiStripCharge->at(ihit),
                        vecDigiStripTime->at(ihit)
                        ));
        }
        clusterStripsInEvent = ClusterStrip::fromDigis(digiStripsInEvent);

        digiPadsInEvent.clear();
        for (int ihit=0; ihit<vecDigiPadX->size(); ihit++) {
            digiPadsInEvent.push_back(DigiPad(
                        vecDigiPadChamber->at(ihit),
                        vecDigiPadX->at(ihit),
                        vecDigiPadY->at(ihit),
                        vecDigiPadCharge->at(ihit),
                        vecDigiPadTime->at(ihit)
                        ));
        }
        clusterPadsInEvent = ClusterPad::fromDigis(digiPadsInEvent);

        /* Assign all clusters to detectors and create rechits */
        for (int icluster=0; icluster<clusterStripsInEvent.size(); icluster++) {

            int chamber = clusterStripsInEvent[icluster].getChamber();
            vecClusterStripChamber.push_back(clusterStripsInEvent[icluster].getChamber());
            vecClusterStripEta.push_back(clusterStripsInEvent[icluster].getEta());
            vecClusterStripCenter.push_back(clusterStripsInEvent[icluster].getCenter());
            vecClusterStripSize.push_back(clusterStripsInEvent[icluster].getSize());

            if (verbose) {
                std::cout << "  Chamber " << chamber;
                std::cout << ", eta " << clusterStripsInEvent[icluster].getEta();
                std::cout << ", center " << clusterStripsInEvent[icluster].getCenter();
                std::cout << ", size " << clusterStripsInEvent[icluster].getSize();
                std::cout << ", charge " << clusterStripsInEvent[icluster].getCharge();
                std::cout << ", time " << clusterStripsInEvent[icluster].getTime();
            }
            // create rechit from cluster on chosen detector:
            rechit = setupGeometry.getChamber(chamber)->createRechit(clusterStripsInEvent[icluster]);
            vecRechitChamber.push_back(chamber);
            vecRechitEta.push_back(clusterStripsInEvent[icluster].getEta());
            vecRechitX.push_back(rechit.getX());
            vecRechitY.push_back(rechit.getY());
            vecRechitErrorX.push_back(rechit.getErrorX());
            vecRechitErrorY.push_back(rechit.getErrorY());
            vecRechitR.push_back(rechit.getR());
            vecRechitPhi.push_back(rechit.getPhi());
            vecRechitClusterSize.push_back(rechit.getClusterSize());
            vecRechitCharge.push_back(rechit.getCharge());
            vecRechitTime.push_back(rechit.getTime());

            if (verbose) {
                std::cout << " local (" << rechit.getX() << ",";
                std::cout << rechit.getY() << ") ";
                std::cout << "± (" << rechit.getErrorX() << ",";
                std::cout << rechit.getErrorY() << ")";

                std::cout << ", charge " << rechit.getCharge();
                std::cout << ", time " << rechit.getTime();
                std::cout << std::endl;
            }
        }
        /**
         * Do the same, but for pad clusters:
         */
        for (int icluster=0; icluster<clusterPadsInEvent.size(); icluster++) {

            int chamber = clusterPadsInEvent[icluster].getChamber();
            vecClusterPadChamber.push_back(clusterPadsInEvent[icluster].getChamber());
            vecClusterPadCenterX.push_back(clusterPadsInEvent[icluster].getCenterX());
            vecClusterPadCenterY.push_back(clusterPadsInEvent[icluster].getCenterY());
            vecClusterStripSize.push_back(clusterPadsInEvent[icluster].getSize());

            if (verbose) {
                std::cout << "  Chamber " << chamber;
                std::cout << ", x " << clusterPadsInEvent[icluster].getCenterX();
                std::cout << ", y " << clusterPadsInEvent[icluster].getCenterY();
                std::cout << ", size " << clusterPadsInEvent[icluster].getSize();
                std::cout << ", charge " << clusterPadsInEvent[icluster].getCharge();
                std::cout << ", time " << clusterPadsInEvent[icluster].getTime();
            }
            // create rechit from cluster on chosen detector:
            rechit = setupGeometry.getChamber(chamber)->createRechit(clusterPadsInEvent[icluster]);
            vecRechitChamber.push_back(chamber);
            vecRechitEta.push_back(clusterPadsInEvent[icluster].getEta());
            vecRechitX.push_back(rechit.getX());
            vecRechitY.push_back(rechit.getY());
            vecRechitErrorX.push_back(rechit.getErrorX());
            vecRechitErrorY.push_back(rechit.getErrorY());
            vecRechitR.push_back(rechit.getR());
            vecRechitPhi.push_back(rechit.getPhi());
            vecRechitClusterSize.push_back(rechit.getClusterSize());
            vecRechitCharge.push_back(rechit.getCharge());
            vecRechitTime.push_back(rechit.getTime());

            if (verbose) {
                std::cout << ", local (" << rechit.getX() << ",";
                std::cout << rechit.getY() << ") ";
                std::cout << "± (" << rechit.getErrorX() << ",";
                std::cout << rechit.getErrorY() << ")";

                std::cout << ", charge " << rechit.getCharge();
                std::cout << ", time " << rechit.getTime();
                std::cout << std::endl;
            }
        }
        rechitTree.Fill();
    }
    std::cout << std::endl;

    rechitTree.Write();
    rechitFile.Close();
    std::cout << "Output file saved to " << ofile << std::endl;
}
