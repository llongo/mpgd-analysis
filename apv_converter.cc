#include <signal.h>

#include <TFile.h>
#include <TTree.h>

#include "CLI11.hpp"

#include "ReadoutMappingAPV.h"
#include "SRSEvent.h"
#include "DigiEvent.h"
#include "progressbar.h"

bool isInterrupted = false;
void interruptHandler(int dummy) {
    isInterrupted = true;
}
bool verbose = false;

class SRSFileReader {

    public:

        SRSFileReader(std::string ifile) {

            srsFile = new TFile(ifile.c_str(), "READ");
            srsTree = (TTree *) srsFile->Get("apv_raw");
            if (verbose) {
                srsTree->Print();
            }

            // SRS variable branches
            srsTree->SetBranchAddress("srsFec", &(m_SRSEvent.srsFec));
            srsTree->SetBranchAddress("srsChip", &(m_SRSEvent.srsChip));
            srsTree->SetBranchAddress("srsChan", &(m_SRSEvent.srsChannel));
            srsTree->SetBranchAddress("max_q", &(m_SRSEvent.maxCharge));
            srsTree->SetBranchAddress("t_max_q", &(m_SRSEvent.timeMaxCharge));
        }

        SRSEvent *readEvent(int eventIndex) {
            srsTree->GetEntry(eventIndex);
            return &m_SRSEvent;
        }

        int getEntries() {
            return srsTree->GetEntries();
        }

    private:

        TFile *srsFile;
        TTree *srsTree;

        SRSEvent m_SRSEvent;

};

class DigiFileWriter {

    public:

        DigiFileWriter(std::string ofile, ReadoutMappingAPV mapping) {
            m_readoutMapping = mapping;

            digiFile = new TFile(ofile.c_str(), "RECREATE", "Digi file");
            digiTree = new TTree("outputtree", "outputtree");

            digiTree->Branch("orbitNumber", &m_digiEvent.orbitNumber);
            digiTree->Branch("bunchCounter", &m_digiEvent.bunchCounter);
            digiTree->Branch("eventCounter", &m_digiEvent.eventCounter);
            digiTree->Branch("runParameter", &m_digiEvent.runParameter);
            digiTree->Branch("pulse_stretch", &m_digiEvent.pulseStretch);
            digiTree->Branch("rawChannel", &m_digiEvent.rawChannel);

            // digi variable branches
            digiTree->Branch("digiStripChamber", m_digiEvent.digiStripChamber);
            digiTree->Branch("digiStripEta", m_digiEvent.digiStripEta);
            digiTree->Branch("digiStrip", m_digiEvent.digiStrip);
            digiTree->Branch("digiStripCharge", m_digiEvent.digiStripCharge);
            digiTree->Branch("digiStripTime", m_digiEvent.digiStripTime);
            digiTree->Branch("digiPadChamber", m_digiEvent.digiPadChamber);
            digiTree->Branch("digiPadX", m_digiEvent.digiPadX);
            digiTree->Branch("digiPadY", m_digiEvent.digiPadY);
            digiTree->Branch("digiPadCharge", m_digiEvent.digiPadCharge);
            digiTree->Branch("digiPadTime", m_digiEvent.digiPadTime);
        }

        ~DigiFileWriter() {
            digiTree->Write();
            digiFile->Close();
        }

        DigiEvent *fromSRS(SRSEvent *srsEvent) {
            m_digiEvent.clear();
            for (int i=0; i<srsEvent->srsFec->size(); i++) {
                FedChipChannelTuple p{srsEvent->srsFec->at(i), srsEvent->srsChip->at(i), srsEvent->srsChannel->at(i)};
                readoutType = m_readoutMapping.getType(p);
                if (readoutType==0) {
                    // Strip readout:
                    m_digiEvent.digiStripChamber->push_back(m_readoutMapping.getChamber(p));
                    m_digiEvent.digiStripEta->push_back(1); // there are no eta partitions detectors read out by SRS
                    m_digiEvent.digiStrip->push_back(m_readoutMapping.getStrip(p));
                    m_digiEvent.digiStripCharge->push_back(srsEvent->maxCharge->at(i));
                    m_digiEvent.digiStripTime->push_back(srsEvent->timeMaxCharge->at(i));
                } else if (readoutType==1) {
                    // Pad readout:
                    m_digiEvent.digiPadChamber->push_back(m_readoutMapping.getChamber(p));
                    m_digiEvent.digiPadX->push_back(m_readoutMapping.getPadX(p));
                    m_digiEvent.digiPadY->push_back(m_readoutMapping.getPadY(p));
                    m_digiEvent.digiPadCharge->push_back(srsEvent->maxCharge->at(i));
                    m_digiEvent.digiPadTime->push_back(srsEvent->timeMaxCharge->at(i));
                } else {
                    throw std::invalid_argument("Readout type " + std::to_string(readoutType) + " not valid.");
                }
            }
            return &m_digiEvent;
        }

        void writeEvent() {
            digiTree->Fill();
        }

    private:

        ReadoutMappingAPV m_readoutMapping;
        TFile *digiFile;
        TTree *digiTree;

        DigiEvent m_digiEvent;

        int readoutType;

};

int main (int argc, char** argv) {

    std::string srs_filename, digi_filename;
    uint64_t max_events{0};
    std::string geometry_name = "hcal-july2023";

    CLI::App app{"Convert SRS output file to digi"};
    app.add_option("input", srs_filename, "SRS input ROOT file path")->required();
    app.add_option("output", digi_filename, "Digi output file path")->required();
    app.add_option("--geometry", geometry_name, "Geometry name");
    app.add_option("-n,--events", max_events, "Maximum number of events to read");
    app.add_flag("-v,--verbose", verbose, "Verbose mode");
    try {
        app.parse(argc, argv);
    } catch (const CLI::ParseError &e) {
        return app.exit(e);
    }

    std::cout << "Reading mapping file..." << std::endl;
    std::string mappingBaseDir = "mapping/"+geometry_name;
    ReadoutMappingAPV readoutMapping(mappingBaseDir+"/mapping.csv");
    std::cout << "Mapping file read." << std::endl;

    if (verbose) {
        std::cout << "Channel mapping:" << std::endl;
        readoutMapping.print();
    }

    SRSFileReader reader{srs_filename};
    DigiFileWriter writer{digi_filename, readoutMapping};

    /**
     * Read SRS event, turn it to digi and save it:
     */
    int nentries;
    if (max_events==0) nentries = reader.getEntries();
    else nentries = max_events;
    SRSEvent *srsEvent;
    DigiEvent *digiEvent;

    progressbar bar(nentries);
    signal(SIGINT, interruptHandler);
    std::cout << "Processing " << nentries << " events..." << std::endl; 
    for (int nentry=0; (!isInterrupted) && nentry<nentries; ++nentry) {

        if (verbose) std::cout << "Event " << nentry << "/" << nentries << std::endl;
        else bar.update();

        srsEvent = reader.readEvent(nentry);
        digiEvent = writer.fromSRS(srsEvent);
        if (verbose) {
            srsEvent->print();
            digiEvent->print();
        }
        writer.writeEvent();

    }

    std::cout << std::endl;
    std::cout << "Output file saved to " << digi_filename << std::endl;
}
