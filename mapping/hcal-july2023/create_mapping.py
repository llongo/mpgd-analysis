import numpy as np
import pandas as pd

""" Combine APV internal mapping (channel to pad) and setup mapping (FEC and chip to chamber and position) """
setup_df = pd.read_csv("setup_mapping.csv", comment="#")
pad_df = pd.read_csv("pad_mapping.csv", comment="#")
tmmx_df = pd.read_csv("tmm_x.csv", comment="#")
tmmy_df = pd.read_csv("tmm_y.csv", comment="#")
apv_df = pd.read_csv("apv.csv", comment="#") # contains Panasonic pin to channel mapping, needed for TMM

# Map TMM Panasonic pins to channel. This is not needed for pad detectors:
tmmx_df = pd.merge(tmmx_df, apv_df, how="inner", on="pin").sort_values(by=["chip", "channel"])
tmmy_df = pd.merge(tmmy_df, apv_df, how="inner", on="pin").sort_values(by=["chip", "channel"])
tmmx_df = tmmx_df[["chip", "channel", "strip"]]
tmmy_df = tmmy_df[["chip", "channel", "strip"]]

# What we call "chip" in the internal mapping is actually the position in the chamber:
pad_df.rename(columns={"chip":"position"}, inplace=True)
tmmx_df.rename(columns={"chip":"position"}, inplace=True)
tmmy_df.rename(columns={"chip":"position"}, inplace=True)

print("TMM x mapping:", tmmx_df, sep="\n")
print("TMM y mapping:", tmmy_df, sep="\n")
print("Pad detector mapping:", pad_df, sep="\n")
print("Setup mapping:", setup_df, sep="\n")

# Apply HRS to Panasonic connector mapping to pad detectors:
def map_hrs(channel_df):
    channel = channel_df.channel
    return (channel+1)*(channel%2==0) + (channel-1)*(channel%2==1)
#pad_df.channel = pad_df.apply(map_hrs, axis=1)
print("Pad detector mapping after HRS mapping:", pad_df, sep="\n")

# Combine the pad dataframe with the setup:
mapping_pad_df = pd.merge(pad_df, setup_df[setup_df.type==1], how="inner", on="position")
# Assign dummy pad:
mapping_pad_df["strip"] = 0

# Combine the strip dataframe with the setup:
mapping_tmmx_df = pd.merge(tmmx_df, setup_df[setup_df.type==2], how="inner", on="position")
mapping_tmmy_df = pd.merge(tmmy_df, setup_df[setup_df.type==3], how="inner", on="position")
mapping_tmm_df = pd.concat([mapping_tmmx_df, mapping_tmmy_df])
# Strip type is 0 (types 1 and 2 were only meaningful for TMM mapping):
mapping_tmm_df.type = 0
# Assign dummy pads:
mapping_tmm_df["pad_x"] = 0
mapping_tmm_df["pad_y"] = 0

print("Pad mapping:", mapping_pad_df, sep="\n")
print("TMM mapping:", mapping_tmm_df, sep="\n")

mapping_df = pd.concat([mapping_pad_df, mapping_tmm_df])
mapping_df = mapping_df[["fec", "chip", "channel", "chamber", "type", "pad_x", "pad_y", "strip"]]
mapping_df.sort_values(by=["fec", "chip", "channel"], inplace=True)

print("Setup mapping:", mapping_df, sep="\n")
mapping_df.to_csv("mapping.csv", sep=",", index=None)
print("Mapping written to file mapping.csv.")
