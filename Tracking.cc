#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <set>
#include <array>
#include <bitset>
#include <signal.h>
#include <math.h>
#include <sys/stat.h>

#include <chrono>
#include <thread>

#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH2D.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>

#include "Rechit.h"
#include "Hit.h"
#include "Track.h"
#include "DetectorGeometry.h"
#include "SetupGeometry.h"

#include "progressbar.h"

bool isInterrupted = false;
void interruptHandler(int dummy) {
    isInterrupted = true;
}

class TrackingReconstruction {

    public:

        TrackingReconstruction(std::string ifile, std::string ofile, std::string geometry) {
            /* Define detector geometries */
            setupGeometry = new SetupGeometry("geometry/setups/"+geometry+".xml");
            nTrackers = setupGeometry->trackerChambers.size();
            m_ifile = ifile;
            m_ofile = ofile;
        }

        /* @brief Reset support and branch variables */
        void reset() {
            for (int layerIndex=0; layerIndex<setupGeometry->layers.size(); layerIndex++) {
                if (setupGeometry->isTrackingLayer(layerIndex)) {
                    hitsPerTrackingLayer[layerIndex] = 0;
                }
            }
            trackerTempTracks.clear();
            rechitLayer.clear();
            rechitChamber.clear();
            rechitEta.clear();
            rechitLocalX.clear();
            rechitLocalY.clear();
            rechitR.clear();
            rechitPhi.clear();
            rechitErrorX.clear();
            rechitErrorY.clear();
            rechitGlobalX.clear();
            rechitGlobalY.clear();
            rechitErrorGlobalX.clear();
            rechitErrorGlobalY.clear();
            rechitClusterSize.clear();
            partialTrackChamber.clear();
            partialTrackChi2.clear();
            partialTrackCovarianceX.clear();
            partialTrackCovarianceY.clear();
            partialTrackSlopeX.clear();
            partialTrackSlopeY.clear();
            partialTrackInterceptX.clear();
            partialTrackInterceptY.clear();
            partialProphitEta.clear();
            partialProphitGlobalX.clear();
            partialProphitGlobalY.clear();
            partialProphitErrorX.clear();
            partialProphitErrorY.clear();
            partialProphitLocalX.clear();
            partialProphitLocalY.clear();
            partialProphitR.clear();
            partialProphitPhi.clear();
            allChi2.clear();
            prophitChamber.clear();
            prophitEta.clear();
            prophitGlobalX.clear();
            prophitGlobalY.clear();
            prophitErrorX.clear();
            prophitErrorY.clear();
            prophitLocalX.clear();
            prophitLocalY.clear();
            prophitR.clear();
            prophitPhi.clear();
        }

        /* @brief Track reconstruction from rechit variables and extrapolation
         *
         * @param list of IDs of the detectors to be used for the reconstruction
         */
        void createTrack(std::vector<int> excludedLayers) {
            /* Building full tracks
             *
             * Create all possible tracks with all tracking detectors,
             * then keep only the track with the lowest chi squared 
             */
            trackerTempTracks.clear();
            allChi2.clear();
            track.clear();

            bool isPartialTrack = excludedLayers.size()>0;

            if (m_verbose) {
                if (isPartialTrack) {
                    std::cout << "    Building track excluding layers ";
                    for (auto l:excludedLayers) std::cout << l << " ";
                    std::cout << std::endl;
                } else {
                    std::cout << "    Building full track" << std::endl;
                }
            }
            // Create array with layers to be used for tracking only:
            std::vector<int> trackingLayers;
            for (int irechit=0; irechit<nrechits; irechit++) {
                layer = rechitLayer.at(irechit);
                if (setupGeometry->isTrackingLayer(layer) && std::find(excludedLayers.begin(), excludedLayers.end(), layer)==std::end(excludedLayers)) {
                    trackingLayers.push_back(layer);
                }
            }
            // Create unique array of chambers:
            std::set<int> layersUniqueSet(trackingLayers.begin(), trackingLayers.end());
            std::vector<int> layersUnique(layersUniqueSet.begin(), layersUniqueSet.end());
            int nTrackersInEvent = layersUnique.size();
            if (m_verbose) {
                std::cout << "      There are " << nTrackersInEvent << " tracking layers in the event: [";
                for (auto l:layersUnique) std::cout << " " << l;
                std::cout << " ]" << std::endl;
            }
            if (nTrackersInEvent < 3) {
                if (m_verbose) {
                    std::cout << "      Not enough tracking layers, skipping event..." << std::endl;
                }
                return;
            }
            // Divide the rechit indices in one vector per tracking layer:
            std::vector<std::vector<int>> rechitIndicesPerLayer(nTrackersInEvent);
            for (int irechit=0; irechit<nrechits; irechit++) {
                chamber = rechitChamber.at(irechit);
                layer = rechitLayer.at(irechit);
                if (!setupGeometry->isTrackingLayer(layer) || std::find(excludedLayers.begin(), excludedLayers.end(), layer)!=std::end(excludedLayers)) continue;
                int layerIndex = std::find(layersUnique.begin(), layersUnique.end(), layer) - layersUnique.begin();
                rechitIndicesPerLayer[layerIndex].push_back(irechit);
            }
            if (m_verbose) {
                for (int i=0; i<rechitIndicesPerLayer.size(); i++) {
                    std::cout << "      Chamber " << layersUnique[i] << ": ";
                    for (auto rechitIndex:rechitIndicesPerLayer[i]) std::cout << rechitIndex << " ";
                    std::cout << std::endl;
                }
            }
            // Create an array with an iterator for each chamber:
            std::vector<std::vector<int>::iterator> iterators;
            for (auto it=rechitIndicesPerLayer.begin(); it!=rechitIndicesPerLayer.end(); it++) {
                iterators.push_back(it->begin());
            }

            // Skip event if too many spurious hits:
            int nPossibleTracks = 1;
            for (auto el:rechitIndicesPerLayer) nPossibleTracks *= el.size();
            if (m_verbose) {
                std::cout << "      There are " << nPossibleTracks << " possible tracks in the event..." << std::endl;
            }
            if (nPossibleTracks < 0 || nPossibleTracks > 50) {
                if (m_verbose) {
                    std::cout << "    Too many possible tracks, skipping event..." << std::endl;
                }
                return;
            }

            /* Create all possible rechit combinations per tracking layer
             * using "odometer" method:
             * https://stackoverflow.com/a/1703575
             */
            while (iterators[0] != rechitIndicesPerLayer[0].end()) {
                // build the track with current rechit combination:
                //std::this_thread::sleep_for(std::chrono::milliseconds(50));
                Track testTrack;
                for (auto it:iterators) {
                    int rechitIndex = *it;
                    chamber = rechitChamber.at(rechitIndex);
                    layer = rechitLayer.at(rechitIndex);
                    rechit = Rechit(
                            chamber,
                            vecRechitX->at(rechitIndex), vecRechitY->at(rechitIndex),
                            vecRechitErrorX->at(rechitIndex), vecRechitErrorY->at(rechitIndex),
                            vecRechitClusterSize->at(rechitIndex)
                            );
                    setupGeometry->getChamber(chamber)->mapRechit(&rechit); // apply local geometry
                    testTrack.addRechit(rechit);
                }
                // Build track and append it to the list:
                testTrack.fit();
                if (!testTrack.isValid()) {
                    trackChi2 = -1.;
                } else {
                    trackChi2 = testTrack.getChi2Reduced();
                }
                trackerTempTracks.push_back(testTrack);
                allChi2.push_back(trackChi2);
                if (m_verbose) {
                    std::cout << "      Built track with rechit IDs: ";
                    for (auto it:iterators) std::cout << *it << " ";
                    std::cout << " and chi2 " << trackChi2 << std::endl;
                }

                iterators[nTrackersInEvent-1]++; // always scan the least significant vector
                for (int iLayer=nTrackersInEvent-1; (iLayer>0) && (iterators[iLayer]==rechitIndicesPerLayer[iLayer].end()); iLayer--) {
                    // if a vector arrived at the end, restart from the beginning
                    // and increment the vector one level higher:
                    iterators[iLayer] = rechitIndicesPerLayer[iLayer].begin();
                    iterators[iLayer-1]++;
                }
            }
            int bestTrackIndex = 0;
            double bestTrackChi2 = allChi2.at(0);
            double presentTrackChi2;
            for (int i=0; i<trackerTempTracks.size(); i++) {
                //presentTrackChi2 = trackerTempTracks.at(i).getChi2ReducedX()+trackerTempTracks.at(i).getChi2ReducedY();
                presentTrackChi2 = allChi2.at(i);
                if (presentTrackChi2<=bestTrackChi2) {
                    bestTrackIndex = i;
                    bestTrackChi2 = presentTrackChi2;
                }
            }
            track = trackerTempTracks.at(bestTrackIndex);
            allChi2.erase(allChi2.begin() + bestTrackIndex);
            if (m_verbose) {
                std::cout << "      Found best track at index " << bestTrackIndex;
                std::cout << " with chi2x " << track.getChi2X();
                std::cout << " and chi2y " << track.getChi2Y() << ". ";
                std::cout << "Slope x " << track.getSlopeX() << ", intercept x " << track.getInterceptX() << ", ";
                std::cout << "slope y " << track.getSlopeY() << ", intercept y " << track.getInterceptY();
                std::cout << std::endl;
            }

            if (!track.isValid()) {
                trackChi2 = -1.;
            } else {
                trackChi2 = track.getChi2Reduced();
                trackCovarianceX = track.getCovarianceX();
                trackCovarianceY = track.getCovarianceY();
                trackSlopeX = track.getSlopeX();
                trackSlopeY = track.getSlopeY();
                trackInterceptX = track.getInterceptX();
                trackInterceptY = track.getInterceptY();

                // Extrapolate track on the other detectors
                for (layer=0; layer<setupGeometry->layers.size(); layer++) {
                    /* Only propagate to non-tracking detectors:
                     * if this is a partial track, propagate only if it is in the excluded list;
                     * if this is a full track, propagate only if it is not a tracking detector.
                     */
                    if (isPartialTrack && std::find(excludedLayers.begin(), excludedLayers.end(), layer)==std::end(excludedLayers)) continue; 
                    else if (!isPartialTrack && setupGeometry->isTrackingLayer(layer)) continue;

                    try {
                        hit = track.propagate(setupGeometry, &setupGeometry->layers.at(layer));
                        chamber = hit.getChamber();
                        if (excludedLayers.size()==0) { // fill track branches
                            prophitChamber.push_back(chamber);
                            prophitEta.push_back(hit.getEta());
                            prophitGlobalX.push_back(hit.getGlobalX());
                            prophitGlobalY.push_back(hit.getGlobalY());
                            prophitErrorX.push_back(hit.getErrorX());
                            prophitErrorY.push_back(hit.getErrorY());
                            prophitLocalX.push_back(hit.getLocalX());
                            prophitLocalY.push_back(hit.getLocalY());
                            prophitR.push_back(hit.getLocalR());
                            prophitPhi.push_back(hit.getLocalPhi());
                        } else { // fill partial track branches
                            partialTrackChamber.push_back(chamber);
                            partialTrackChi2.push_back(trackChi2);
                            partialTrackCovarianceX.push_back(trackCovarianceX);
                            partialTrackCovarianceY.push_back(trackCovarianceY);
                            partialTrackSlopeX.push_back(trackSlopeX);
                            partialTrackSlopeY.push_back(trackSlopeY);
                            partialTrackInterceptX.push_back(trackInterceptX);
                            partialTrackInterceptY.push_back(trackInterceptY);
                            partialProphitEta.push_back(hit.getEta());
                            partialProphitGlobalX.push_back(hit.getGlobalX());
                            partialProphitGlobalY.push_back(hit.getGlobalY());
                            partialProphitErrorX.push_back(hit.getErrorX());
                            partialProphitErrorY.push_back(hit.getErrorY());
                            partialProphitLocalX.push_back(hit.getLocalX());
                            partialProphitLocalY.push_back(hit.getLocalY());
                            partialProphitR.push_back(hit.getLocalR());
                            partialProphitPhi.push_back(hit.getLocalPhi());
                        }

                        if (m_verbose) {
                            std::cout << "      Track extrapolated to layer " << layer << " ";
                            std::cout << "ended on to chamber " << chamber << ": " << std::endl;
                            std::cout << "        " << "Track slope (" << track.getSlopeX() << "," << track.getSlopeY() << ")";
                            std::cout << " " << "intercept (" << track.getInterceptX() << "," << track.getInterceptY() << ")";
                            std::cout << std::endl;
                            std::cout << "        " << "Prophit " << "eta=" << hit.getEta() << ", ";
                            std::cout << "global carthesian (" << hit.getGlobalX() << "," << hit.getGlobalY() << "), ";
                            std::cout << "local carthesian (" << hit.getLocalX() << "," << hit.getLocalY() << "), ";
                            std::cout << "polar R=" << hit.getLocalR() << ", phi=" << hit.getLocalPhi();
                            std::cout << std::endl;
                        }
                    } catch (const std::domain_error& e) {
                        /* The hit fell outside the chamber, so we skip it */
                        if (m_verbose) {
                            std::cout << "      Hit fell outside of chamber borders: ";
                            std::cout << "global carthesian (" << hit.getGlobalX() << "," << hit.getGlobalY() << ")";
                            std::cout << std::endl;;
                        }
                    }
                }
            }
        }

        void reco(int max_events, bool verbose) {

            m_verbose = verbose;

            TFile rechitFile(m_ifile.c_str(), "READ");     
            TTree *rechitTree = (TTree *) rechitFile.Get("rechitTree");

            TFile trackFile(m_ofile.c_str(), "RECREATE", "Track file");
            TTree trackTree("trackTree", "trackTree");

            /** Rechit variables from the input tree **/
            vecRechitChamber = new std::vector<int>();
            vecRechitEta = new std::vector<int>();
            vecRechitX = new std::vector<double>();
            vecRechitY = new std::vector<double>();
            vecRechitErrorX = new std::vector<double>();
            vecRechitErrorY = new std::vector<double>();
            vecRechitR = new std::vector<double>();
            vecRechitPhi = new std::vector<double>();
            vecRechitClusterSize = new std::vector<double>();
            vecRechitCharge = new std::vector<int>();
            vecRechitTime = new std::vector<int>();
            vecRawChannel = new std::vector<int>();

            // event branches
            rechitTree->SetBranchAddress("orbitNumber", &orbitNumber);
            rechitTree->SetBranchAddress("bunchCounter", &bunchCounter);
            rechitTree->SetBranchAddress("eventCounter", &eventCounter);

            // rechit branches
            rechitTree->SetBranchAddress("rechitChamber", &vecRechitChamber);
            rechitTree->SetBranchAddress("rechitEta", &vecRechitEta);
            rechitTree->SetBranchAddress("rechitX", &vecRechitX);
            rechitTree->SetBranchAddress("rechitY", &vecRechitY);
            rechitTree->SetBranchAddress("rechitErrorX", &vecRechitErrorX);
            rechitTree->SetBranchAddress("rechitErrorY", &vecRechitErrorY);
            rechitTree->SetBranchAddress("rechitR", &vecRechitR);
            rechitTree->SetBranchAddress("rechitPhi", &vecRechitPhi);
            rechitTree->SetBranchAddress("rechitClusterSize", &vecRechitClusterSize);
            rechitTree->SetBranchAddress("rechitCharge", &vecRechitCharge);
            rechitTree->SetBranchAddress("rechitTime", &vecRechitTime);
            rechitTree->SetBranchAddress("rawChannel", &vecRawChannel);

            // Event branches
            trackTree.Branch("orbitNumber", &orbitNumber);
            trackTree.Branch("bunchCounter", &bunchCounter);
            trackTree.Branch("eventCounter", &eventCounter);
            // Rechit branches propagated from rechit tree with no change
            trackTree.Branch("rechitRawChannel", vecRawChannel);
            // Rechit branches for new variables
            trackTree.Branch("rechitChamber", &rechitChamber);
            trackTree.Branch("rechitEta", &rechitEta);
            trackTree.Branch("rechitLocalX", &rechitLocalX);
            trackTree.Branch("rechitLocalY", &rechitLocalY);
            trackTree.Branch("rechitR", &rechitR);
            trackTree.Branch("rechitPhi", &rechitPhi);
            trackTree.Branch("rechitErrorX", &rechitErrorX);
            trackTree.Branch("rechitErrorY", &rechitErrorY);
            trackTree.Branch("rechitGlobalX", &rechitGlobalX);
            trackTree.Branch("rechitGlobalY", &rechitGlobalY);
            trackTree.Branch("rechitErrorGlobalX", &rechitErrorGlobalX);
            trackTree.Branch("rechitErrorGlobalY", &rechitErrorGlobalY);
            trackTree.Branch("rechitClusterSize", &rechitClusterSize);
            // Partial track branches
            trackTree.Branch("partialTrackChamber", &partialTrackChamber);
            trackTree.Branch("partialTrackChi2", &partialTrackChi2);
            trackTree.Branch("partialTrackCovarianceX", &partialTrackCovarianceX);
            trackTree.Branch("partialTrackCovarianceY", &partialTrackCovarianceY);
            trackTree.Branch("partialTrackSlopeX", &partialTrackSlopeX);
            trackTree.Branch("partialTrackSlopeY", &partialTrackSlopeY);
            trackTree.Branch("partialTrackInterceptX", &partialTrackInterceptX);
            trackTree.Branch("partialTrackInterceptY", &partialTrackInterceptY);
            trackTree.Branch("partialProphitEta", &partialProphitEta);
            trackTree.Branch("partialProphitGlobalX", &partialProphitGlobalX);
            trackTree.Branch("partialProphitGlobalY", &partialProphitGlobalY);
            trackTree.Branch("partialProphitErrorX", &partialProphitErrorX);
            trackTree.Branch("partialProphitErrorY", &partialProphitErrorY);
            trackTree.Branch("partialProphitLocalX", &partialProphitLocalX);
            trackTree.Branch("partialProphitLocalY", &partialProphitLocalY);
            trackTree.Branch("partialProphitR", &partialProphitR);
            trackTree.Branch("partialProphitPhi", &partialProphitPhi);
            // Tracks build with all tracking detectors: 
            trackTree.Branch("trackChi2", &trackChi2, "trackChi2/D");
            trackTree.Branch("trackCovarianceX", &trackCovarianceX, "trackCovarianceX/D");
            trackTree.Branch("trackCovarianceY", &trackCovarianceY, "trackCovarianceY/D");
            trackTree.Branch("trackSlopeX", &trackSlopeX, "trackSlopeX/D");
            trackTree.Branch("trackSlopeY", &trackSlopeY, "trackSlopeY/D");
            trackTree.Branch("trackInterceptX", &trackInterceptX, "trackInterceptX/D");
            trackTree.Branch("trackInterceptY", &trackInterceptY, "trackInterceptY/D");
            trackTree.Branch("allChi2", &allChi2);
            trackTree.Branch("prophitChamber", &prophitChamber);
            trackTree.Branch("prophitEta", &prophitEta);
            trackTree.Branch("prophitGlobalX", &prophitGlobalX);
            trackTree.Branch("prophitGlobalY", &prophitGlobalY);
            trackTree.Branch("prophitErrorX", &prophitErrorX);
            trackTree.Branch("prophitErrorY", &prophitErrorY);
            trackTree.Branch("prophitLocalX", &prophitLocalX);
            trackTree.Branch("prophitLocalY", &prophitLocalY);
            trackTree.Branch("prophitR", &prophitR);
            trackTree.Branch("prophitPhi", &prophitPhi);

            int nentries = rechitTree->GetEntries();
            int nentriesGolden = 0, nentriesSaved = 0;

            std::cout << nentries << " total events" <<  std::endl;
            if (max_events>0) nentries = max_events;
            progressbar bar(nentries);
            signal(SIGINT, interruptHandler);
            for (int nevt=0; (!isInterrupted) && rechitTree->LoadTree(nevt)>=0; ++nevt) {
                if ((max_events>0) && (nevt>max_events)) break;

                if (m_verbose) std::cout << "Event " << nevt << "/" << nentries << std::endl;
                else bar.update();

                reset();

                rechitTree->GetEntry(nevt);

                /**
                 * Clean up event
                 * by keeping for each tracker only the rechit with the highest charge
                 */
                /*std::map<int, int> trackerChargeMap; // holds charge of rechit with minimum charge
                std::map<int, int> trackerIndexMap; // holds index of rechit with minimum charge
                for (int i=0; i<vecRechitChamber->size(); i++) {
                    chamber = vecRechitChamber->at(i);
                    // check only for tracking detectors:
                    if (!setupGeometry->isTrackingChamber(chamber)) continue;
                    if (trackerChargeMap.count(chamber)>0) {
                        // check if present rechit has larger charge:
                        if (vecRechitCharge->at(i)>trackerChargeMap.at(chamber)) {
                            // update charge and index maps:
                            trackerChargeMap[chamber] = vecRechitCharge->at(i);
                            trackerIndexMap[chamber] = i;
                        }
                    } else {
                        // first occurrence of hit in this chamber, so we put it in the map:
                        trackerChargeMap[chamber] = vecRechitCharge->at(i);
                        trackerIndexMap[chamber] = i;
                    }
                }

                // delete all other elements:
                for (int i=0; i<vecRechitChamber->size(); i++) {
                    chamber = vecRechitChamber->at(i);
                    // check only for tracking detectors:
                    if (!setupGeometry->isTrackingChamber(chamber)) continue;
                    if (i!=trackerIndexMap[chamber]) {
                        // delete this rechit:
                        vecRechitChamber->erase(vecRechitChamber->begin()+i);
                        vecRechitEta->erase(vecRechitEta->begin()+i);
                        vecRechitX->erase(vecRechitX->begin()+i);
                        vecRechitY->erase(vecRechitY->begin()+i);
                        vecRechitErrorX->erase(vecRechitErrorX->begin()+i);
                        vecRechitErrorY->erase(vecRechitErrorY->begin()+i);
                        vecRechitR->erase(vecRechitR->begin()+i);
                        vecRechitPhi->erase(vecRechitPhi->begin()+i);
                        vecRechitClusterSize->erase(vecRechitClusterSize->begin()+i);
                        vecRechitCharge->erase(vecRechitCharge->begin()+i);
                        vecRechitTime->erase(vecRechitTime->begin()+i);
                        // all the indexes of the hits to keep have to be updated:
                        for (auto trackerIndexPair : trackerIndexMap) {
                            if (trackerIndexPair.second>i) {
                                trackerIndexMap[trackerIndexPair.first]--;
                            }
                        }
                        // if we deleted an element we don't need to increase index
                        i--;
                    } 
                }*/
                nrechits = vecRechitChamber->size();

                /* Save all rechit local and global coordinates */
                for (int irechit=0; irechit<vecRechitChamber->size(); irechit++) {
                    chamber = vecRechitChamber->at(irechit);
                    hit = Hit::fromLocal(setupGeometry->getChamber(chamber),
                            vecRechitX->at(irechit), vecRechitY->at(irechit),
                            vecRechitErrorX->at(irechit), vecRechitErrorY->at(irechit), 0.
                            );
                    rechitChamber.push_back(chamber);
                    rechitEta.push_back(vecRechitEta->at(irechit));
                    rechitLocalX.push_back(hit.getLocalX());
                    rechitLocalY.push_back(hit.getLocalY());
                    rechitR.push_back(hit.getLocalR());
                    rechitPhi.push_back(hit.getLocalPhi());
                    rechitGlobalX.push_back(hit.getGlobalX());
                    rechitGlobalY.push_back(hit.getGlobalY());
                    rechitErrorX.push_back(hit.getErrorX());
                    rechitErrorY.push_back(hit.getErrorY());
                    rechitClusterSize.push_back(vecRechitClusterSize->at(irechit));

                    /* In addition, also calculate rechit layer.
                     * This is determined by the setup geometry and the chamber ID
                     * and is used to create tracks
                     * but is not saved in a branch */
                    rechitLayer.push_back(setupGeometry->getLayer(chamber));

                    if (m_verbose) {
                        std::cout << "  Layer " << rechitLayer.back() << ", ";
                        std::cout << "chamber " << chamber << ", ";
                        std::cout << "eta " << rechitEta.back() << ", ";
                        std::cout << "global carthesian (" << rechitGlobalX.back() << "," << rechitGlobalY.back() << "), ";
                        std::cout << "local carthesian (" << hit.getLocalX() << "," << hit.getLocalY() << "), ";
                        std::cout << "local polar R=" << hit.getLocalR() << ", phi=" << hit.getLocalPhi();
                        std::cout << std::endl;
                    }
                }

                if (m_verbose) {
                    std::cout << "  #### Extrapolation on tracker ####" << std::endl;
                }
                for (int irechit=0; irechit<nrechits; irechit++) {
                    chamber = rechitChamber.at(irechit);
                    layer = rechitLayer.at(irechit);
                    if (setupGeometry->isTrackingLayer(layer)) {
                        hitsPerTrackingLayer[layer]++;
                    }
                }
                if (m_verbose) {
                    for (auto pair:hitsPerTrackingLayer) {
                        std::cout << "    " << pair.second << " rechits in layer " << pair.first << std::endl;
                    }
                }

                /* Iterate on tracking layers, exclude them
                 * and reconstruct track on remaining ones.
                 * Extrapolate track on all the excluded layers */
                for (int excludedLayer=0; excludedLayer<setupGeometry->layers.size(); excludedLayer++) {
                    if (setupGeometry->isTrackingLayer(excludedLayer)) {
                        createTrack(std::vector<int>{excludedLayer});
                    }
                }
                    
                /* Create track using all tracking detectors: */
                if (m_verbose) {
                    std::cout << "  #### Extrapolation on non-tracking detectors ####" << std::endl;
                }
                createTrack(std::vector<int>(0));

                trackTree.Fill();
                nentriesSaved++;
            }
            std::cout << std::endl;
            std::cout << "Saved entries: " << nentriesSaved << std::endl;

            trackTree.Write();
            trackFile.Close();
            std::cout << "Output files written to " << m_ofile << std::endl;
        }

    private:

        SetupGeometry *setupGeometry;
        int nTrackers;
        bool m_verbose;

        std::string m_ifile, m_ofile;

        /** Rechit variables from the input tree **/
        int nrechits;
        int orbitNumber, bunchCounter, eventCounter;
        std::vector<int> *vecRechitChamber, *vecRechitEta;
        std::vector<double> *vecRechitX, *vecRechitY, *vecRechitErrorX, *vecRechitErrorY, *vecRechitR, *vecRechitPhi, *vecRechitClusterSize; 
        std::vector<int> *vecRechitCharge, *vecRechitTime;
        std::vector<int> *vecRawChannel; 

        /* Track variables for the output tree
         * Divided in three groups:
         * - rechit variables
         * - partial track variables and related prophits
         * - full track variables and related prophits
         */

        /** Rechits **/
        std::vector<int> rechitLayer;
        std::vector<int> rechitChamber;
        std::vector<double> rechitEta;
        std::vector<double> rechitLocalX;
        std::vector<double> rechitLocalY;
        std::vector<double> rechitR;
        std::vector<double> rechitPhi;
        std::vector<double> rechitErrorX;
        std::vector<double> rechitErrorY;
        std::vector<double> rechitGlobalX;
        std::vector<double> rechitGlobalY;
        std::vector<double> rechitErrorGlobalX;
        std::vector<double> rechitErrorGlobalY;
        std::vector<double> rechitClusterSize;

        /** Partial tracks **/
        std::vector<int> partialTrackChamber; // chamber excluded from the track
        std::vector<double> partialTrackChi2;
        std::vector<double> partialTrackCovarianceX, partialTrackCovarianceY;
        std::vector<double> partialTrackSlopeX, partialTrackSlopeY;
        std::vector<double> partialTrackInterceptX, partialTrackInterceptY;
        std::vector<double> partialProphitEta;
        std::vector<double> partialProphitGlobalX, partialProphitGlobalY;
        std::vector<double> partialProphitErrorX, partialProphitErrorY;
        std::vector<double> partialProphitLocalX, partialProphitLocalY;
        std::vector<double> partialProphitR, partialProphitPhi;

        /** Tracks built using all tracking detectors **/
        double trackChi2;
        double trackCovarianceX, trackCovarianceY;
        double trackSlopeX, trackSlopeY;
        double trackInterceptX, trackInterceptY;
        std::vector<double> allChi2; // contains chi2 of discarded tracks as well
        std::vector<int> prophitChamber;
        std::vector<double> prophitEta;
        std::vector<double> prophitGlobalX;
        std::vector<double> prophitGlobalY;
        std::vector<double> prophitErrorX;
        std::vector<double> prophitErrorY;
        std::vector<double> prophitLocalX;
        std::vector<double> prophitLocalY;
        std::vector<double> prophitR;
        std::vector<double> prophitPhi;

        /** Support variabes: **/
        int layer;
        int chamber;
        double rechitX, rechitY;
        double rechitXError, rechitYError;
        double rechitClusterSizeX, rechitClusterSizeY;
        double prophitX, prophitY;
        double prophitXError, prophitYError;
        Rechit rechit;
        Hit hit;
        Track track;
        // Will hold all possible tracks built with all tracker, choose only one at the end:
        std::vector<Track> trackerTempTracks; 
        // Support map from chamber ID to number of hits to exclude events with more than one hit per tracker:
        std::map<int, double> hitsPerTrackingLayer;

};

int main (int argc, char** argv) {

    if (argc<3) {
        std::cout << "Usage: Tracks ifile ofile [--geometry geometry_name] [--verbose] [--events n] [-x corrections x] [-y corrections y] [--angles correction angles]" << std::endl;
        return 0;
    }
    std::string ifile = argv[1];
    std::string ofile = argv[2];

    int max_events = -1;
    bool verbose = false;
    std::string geometry;
    for (int iarg=0; iarg<argc; iarg++) {
        std::string arg = argv[iarg];
        if (arg=="--verbose") verbose = true;
        else if (arg=="--events") max_events = atoi(argv[iarg+1]);
        else if (arg=="--geometry") geometry = argv[iarg+1];
    }

    if (max_events > 0) std::cout << "Analyzing " << max_events << " events" << std::endl;
    else std::cout << "Analyzing all events" << std::endl; 

    TrackingReconstruction reco(ifile, ofile, geometry);
    reco.reco(max_events, verbose);
}
