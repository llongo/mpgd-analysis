# User instructions

These instructions are tested with the ME0 stack test beams and with the MPGD-HCAL test beam of July 2023. For other setups the steps might be different.

## Setting up the environment

```bash
git clone https://gitlab.cern.ch/apellecc/mpgd-analysis.git
cd mpgd-analysis
mkdir build && cd build
source ../env.sh # might complain about ROOT installation if you are not on lxplus; can be ignored
cmake3 ..
make
```

Hint: run `cmake` with the `CMAKE_CXX_FLAGS=-O[l]` option to enable compiler optimization at level `l`.

## Supported geometries

A list of the presently supported detector geometries follows:

| Setup                             | Geometry name        |
| --------------------------------- | -------------------- |
| CMS GEM test beam 2021            | `oct2021`            |
| CMS GEM test beam May 2022        | `may2022`            |
| CMS ME0 GIF++ test beam July 2022 | `july2022`           |
| ME0 stack (4 layers)              | `stack-tb-april2023` |
| MPGD-HCAL at 2022 SPS test beam   | `hcal-july2023`      |

The geometry name has to be passed to each reconstruction step using the `--geometry [geometry_name]` command line argument.

## Adding a channel mask file

Save the mask file in the path `masks/[geometry_name].csv`.

Afterwards, recompile with `make`.

## Running the reconstruction code

The reconstruction consists of three steps:
1. digitization and mapping. This step is different between:
    - GEM VFAT data: raw binary data have to be decoded by an unpacker;
    - SRS APV data: the SRS ROOT file created by mmDAQ has to be converted do the digi format;
2. cluster reconstruction: clusters together neighbouring strips or pad and applies local detector geometry;
3. track reconstruction: applies global detector geometry, reconstructs tracks using chosen tracking detectors and extrapolates them to detectors under test.
Only one track per event is created. In events where multiple tracks are possibile (e.g. with more than one rechit per tracking detector), the following criteria are used:
    - for GEM VFAT setups, all possible tracks are built and only the one with the best chi squared is saved.
    - for SRS APV setups, tracks are built using the TMMs; for each TMM readout plane (x and y), only the rechit with the highest charge is kept and used to build the track,
    while the others are discarded.

More details on the reconstruction (under constant update) are in this presentation: https://docs.google.com/presentation/d/1p60Ddj4Xe4NQAYWLp28O207NohF_f0jMai_7c54Ymxs/edit?usp=sharing.

### 1. Digitization and mapping

##### SRS APV data

To convert the SRS data to "digi" (i.e. mapped strip and pad positions):
```bash
./ApvToDigi [srs ROOT file] [digi ROOT file] --geometry geometry_name
```

##### GEM VFAT data

To unpack raw VFAT data and apply the mapping:
```bash
./RawToDigi --input [raw files or compressed raw files] --output [digi ROOT file ] --geometry geometry_name 
```

### 2. Cluster reconstruction

To reconstruct strip and pad clusters and rechits:
```bash
./DigiToRechits [digi ROOT file] [rechit ROOT file] --geometry geometry_name
```
Rechits are strips and pad clusters mapped to the detector geometry.

### 3. Track reconstruction
```bash
Tracking [rechit ROOT file] [track ROOT file] --geometry geometry_name
```

All commands also have the following options:
- `--events [n]` to run on a limited number of events;
- `--verbose` for increased logging.

### 4. Analysis

Under update. Look at the README file in the `analysis` folder.

# How to contribute to the code

1. Create a branch for your modifications. Use consistent branch namings, for example:
```bash
git checkout -B feature/channel-mask-may2022
```

2. Add the files you have modified, such as:
```bash
git add ../masks/may2022.csv
```

3. Commit your modifications, e.g.:
```bash
git commit -m "Add channel masking for May 2022 test beam"
```

4. Push the changes, then open a merge request following the instructions on the terminal:
```bash
git push origin feature/channel-mask-may2022
```

